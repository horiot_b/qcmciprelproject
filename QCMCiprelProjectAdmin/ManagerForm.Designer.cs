﻿using System;
using QCMCiprelProject;

namespace QCMCiprelProjectAdmin
{
    partial class ManagerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ManagerForm));
            this.qCMCiprelProjectDataSet = new QCMCiprelProjectAdmin.QCMCiprelProjectDataSet();
            this.qCMCiprelProjectDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.categoryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.answerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.answerTableAdapter = new QCMCiprelProjectAdmin.QCMCiprelProjectDataSetTableAdapters.AnswerTableAdapter();
            this.categoryTableAdapter = new QCMCiprelProjectAdmin.QCMCiprelProjectDataSetTableAdapters.CategoryTableAdapter();
            this.categoryDataGrid = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeleteCategory = new System.Windows.Forms.DataGridViewButtonColumn();
            this.questionDataGrid = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.categoryIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.levelDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.image = new System.Windows.Forms.DataGridViewImageColumn();
            this.commentQuestionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.commentCorrectAnswerDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.commentIncorrectAnswerDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addImage = new System.Windows.Forms.DataGridViewButtonColumn();
            this.removeImage = new System.Windows.Forms.DataGridViewButtonColumn();
            this.DeleteQuestion = new System.Windows.Forms.DataGridViewButtonColumn();
            this.isErrorGame = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.questionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.questionTableAdapter = new QCMCiprelProjectAdmin.QCMCiprelProjectDataSetTableAdapters.QuestionTableAdapter();
            this.answerDataGrid = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.saveButton = new System.Windows.Forms.Button();
            this.mainMenuButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.addErrorGameQuestion = new System.Windows.Forms.Button();
            this.addNormalQuestion = new System.Windows.Forms.Button();
            this.saveSuccessLabel = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.idDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.questionIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contentDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.correctAnswerDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.DeleteAnswer = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.qCMCiprelProjectDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qCMCiprelProjectDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.answerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoryDataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.questionDataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.questionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.answerDataGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // qCMCiprelProjectDataSet
            // 
            this.qCMCiprelProjectDataSet.DataSetName = "QCMCiprelProjectDataSet";
            this.qCMCiprelProjectDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // qCMCiprelProjectDataSetBindingSource
            // 
            this.qCMCiprelProjectDataSetBindingSource.DataSource = this.qCMCiprelProjectDataSet;
            this.qCMCiprelProjectDataSetBindingSource.Position = 0;
            // 
            // categoryBindingSource
            // 
            this.categoryBindingSource.DataMember = "Category";
            this.categoryBindingSource.DataSource = this.qCMCiprelProjectDataSetBindingSource;
            // 
            // answerBindingSource
            // 
            this.answerBindingSource.DataMember = "Answer";
            this.answerBindingSource.DataSource = this.qCMCiprelProjectDataSetBindingSource;
            // 
            // answerTableAdapter
            // 
            this.answerTableAdapter.ClearBeforeFill = true;
            // 
            // categoryTableAdapter
            // 
            this.categoryTableAdapter.ClearBeforeFill = true;
            // 
            // categoryDataGrid
            // 
            this.categoryDataGrid.AllowUserToDeleteRows = false;
            this.categoryDataGrid.AutoGenerateColumns = false;
            this.categoryDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.categoryDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn,
            this.DeleteCategory});
            this.categoryDataGrid.DataSource = this.categoryBindingSource;
            this.categoryDataGrid.Location = new System.Drawing.Point(12, 12);
            this.categoryDataGrid.MultiSelect = false;
            this.categoryDataGrid.Name = "categoryDataGrid";
            this.categoryDataGrid.Size = new System.Drawing.Size(257, 394);
            this.categoryDataGrid.TabIndex = 0;
            this.categoryDataGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.categoryDataGrid_CellContentClick);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Identifiant";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            this.idDataGridViewTextBoxColumn.Width = 55;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Nom de la catégorie*";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            // 
            // DeleteCategory
            // 
            this.DeleteCategory.HeaderText = "Supprimer la catégorie";
            this.DeleteCategory.Name = "DeleteCategory";
            this.DeleteCategory.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DeleteCategory.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.DeleteCategory.Text = "Delete";
            this.DeleteCategory.UseColumnTextForButtonValue = true;
            this.DeleteCategory.Width = 70;
            // 
            // questionDataGrid
            // 
            this.questionDataGrid.AllowUserToDeleteRows = false;
            this.questionDataGrid.AutoGenerateColumns = false;
            this.questionDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.questionDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn1,
            this.contentDataGridViewTextBoxColumn,
            this.categoryIdDataGridViewTextBoxColumn,
            this.levelDataGridViewTextBoxColumn,
            this.image,
            this.commentQuestionDataGridViewTextBoxColumn,
            this.commentCorrectAnswerDataGridViewTextBoxColumn,
            this.commentIncorrectAnswerDataGridViewTextBoxColumn,
            this.addImage,
            this.removeImage,
            this.DeleteQuestion,
            this.isErrorGame});
            this.questionDataGrid.DataSource = this.questionBindingSource;
            this.questionDataGrid.Location = new System.Drawing.Point(275, 12);
            this.questionDataGrid.MultiSelect = false;
            this.questionDataGrid.Name = "questionDataGrid";
            this.questionDataGrid.Size = new System.Drawing.Size(982, 253);
            this.questionDataGrid.TabIndex = 1;
            this.questionDataGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.questionDataGrid_CellContentClick);
            // 
            // idDataGridViewTextBoxColumn1
            // 
            this.idDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.idDataGridViewTextBoxColumn1.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn1.HeaderText = "Identifiant";
            this.idDataGridViewTextBoxColumn1.Name = "idDataGridViewTextBoxColumn1";
            this.idDataGridViewTextBoxColumn1.ReadOnly = true;
            this.idDataGridViewTextBoxColumn1.Width = 60;
            // 
            // contentDataGridViewTextBoxColumn
            // 
            this.contentDataGridViewTextBoxColumn.DataPropertyName = "content";
            this.contentDataGridViewTextBoxColumn.HeaderText = "Question*";
            this.contentDataGridViewTextBoxColumn.Name = "contentDataGridViewTextBoxColumn";
            this.contentDataGridViewTextBoxColumn.Width = 200;
            // 
            // categoryIdDataGridViewTextBoxColumn
            // 
            this.categoryIdDataGridViewTextBoxColumn.DataPropertyName = "categoryId";
            this.categoryIdDataGridViewTextBoxColumn.HeaderText = "Identifiant de la catégorie*";
            this.categoryIdDataGridViewTextBoxColumn.Name = "categoryIdDataGridViewTextBoxColumn";
            this.categoryIdDataGridViewTextBoxColumn.Width = 65;
            // 
            // levelDataGridViewTextBoxColumn
            // 
            this.levelDataGridViewTextBoxColumn.DataPropertyName = "level";
            this.levelDataGridViewTextBoxColumn.HeaderText = "Difficulté (entre 1 et 3)*";
            this.levelDataGridViewTextBoxColumn.Name = "levelDataGridViewTextBoxColumn";
            this.levelDataGridViewTextBoxColumn.Width = 65;
            // 
            // image
            // 
            this.image.DataPropertyName = "image";
            this.image.HeaderText = "Image";
            this.image.Name = "image";
            this.image.Width = 50;
            // 
            // commentQuestionDataGridViewTextBoxColumn
            // 
            this.commentQuestionDataGridViewTextBoxColumn.DataPropertyName = "commentQuestion";
            this.commentQuestionDataGridViewTextBoxColumn.HeaderText = "Commentaire";
            this.commentQuestionDataGridViewTextBoxColumn.Name = "commentQuestionDataGridViewTextBoxColumn";
            // 
            // commentCorrectAnswerDataGridViewTextBoxColumn
            // 
            this.commentCorrectAnswerDataGridViewTextBoxColumn.DataPropertyName = "commentCorrectAnswer";
            this.commentCorrectAnswerDataGridViewTextBoxColumn.HeaderText = "Commentaire bonnne réponse";
            this.commentCorrectAnswerDataGridViewTextBoxColumn.Name = "commentCorrectAnswerDataGridViewTextBoxColumn";
            // 
            // commentIncorrectAnswerDataGridViewTextBoxColumn
            // 
            this.commentIncorrectAnswerDataGridViewTextBoxColumn.DataPropertyName = "commentIncorrectAnswer";
            this.commentIncorrectAnswerDataGridViewTextBoxColumn.HeaderText = "Commentaire mauvaise réponse";
            this.commentIncorrectAnswerDataGridViewTextBoxColumn.Name = "commentIncorrectAnswerDataGridViewTextBoxColumn";
            // 
            // addImage
            // 
            this.addImage.HeaderText = "Ajouter / Modifier l\'image";
            this.addImage.Name = "addImage";
            this.addImage.Text = "Add / Edit";
            this.addImage.UseColumnTextForButtonValue = true;
            this.addImage.Width = 70;
            // 
            // removeImage
            // 
            this.removeImage.HeaderText = "Supprimer l\'image";
            this.removeImage.Name = "removeImage";
            this.removeImage.Text = "Delete";
            this.removeImage.UseColumnTextForButtonValue = true;
            this.removeImage.Width = 70;
            // 
            // DeleteQuestion
            // 
            this.DeleteQuestion.HeaderText = "Supprimer la question";
            this.DeleteQuestion.Name = "DeleteQuestion";
            this.DeleteQuestion.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DeleteQuestion.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.DeleteQuestion.Text = "Delete";
            this.DeleteQuestion.UseColumnTextForButtonValue = true;
            this.DeleteQuestion.Width = 70;
            // 
            // isErrorGame
            // 
            this.isErrorGame.DataPropertyName = "isErrorGame";
            this.isErrorGame.HeaderText = "isErrorGame";
            this.isErrorGame.Name = "isErrorGame";
            this.isErrorGame.Visible = false;
            // 
            // questionBindingSource
            // 
            this.questionBindingSource.DataMember = "Question";
            this.questionBindingSource.DataSource = this.qCMCiprelProjectDataSetBindingSource;
            // 
            // questionTableAdapter
            // 
            this.questionTableAdapter.ClearBeforeFill = true;
            // 
            // answerDataGrid
            // 
            this.answerDataGrid.AutoGenerateColumns = false;
            this.answerDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.answerDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn2,
            this.questionIdDataGridViewTextBoxColumn,
            this.contentDataGridViewTextBoxColumn1,
            this.correctAnswerDataGridViewCheckBoxColumn,
            this.DeleteAnswer});
            this.answerDataGrid.DataSource = this.answerBindingSource;
            this.answerDataGrid.Location = new System.Drawing.Point(275, 271);
            this.answerDataGrid.MultiSelect = false;
            this.answerDataGrid.Name = "answerDataGrid";
            this.answerDataGrid.Size = new System.Drawing.Size(501, 293);
            this.answerDataGrid.TabIndex = 2;
            this.answerDataGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.answerDataGrid_CellContentClick);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(782, 271);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(475, 75);
            this.label1.TabIndex = 3;
            this.label1.Text = resources.GetString("label1.Text");
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(12, 520);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(132, 44);
            this.saveButton.TabIndex = 4;
            this.saveButton.Text = "Enregistrer";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // mainMenuButton
            // 
            this.mainMenuButton.Location = new System.Drawing.Point(150, 520);
            this.mainMenuButton.Name = "mainMenuButton";
            this.mainMenuButton.Size = new System.Drawing.Size(119, 44);
            this.mainMenuButton.TabIndex = 5;
            this.mainMenuButton.Text = "Menu principal";
            this.mainMenuButton.UseVisualStyleBackColor = true;
            this.mainMenuButton.Click += new System.EventHandler(this.mainMenuButton_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(782, 393);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(475, 114);
            this.label2.TabIndex = 6;
            this.label2.Text = resources.GetString("label2.Text");
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.DarkRed;
            this.label3.Location = new System.Drawing.Point(782, 371);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "/!\\ ATTENTION /!\\";
            // 
            // label4
            // 
            this.label4.ForeColor = System.Drawing.Color.DarkRed;
            this.label4.Location = new System.Drawing.Point(782, 504);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(475, 30);
            this.label4.TabIndex = 8;
            this.label4.Text = "LE NON RESPECT DE CES REGLES ENTRAINERA LA PERTE DES MODIFICATIONS NON ENREGISTRE" +
    "ES ET PEUX INDUIRE A UN MAUVAIS CLASSEMENT DES QUESTIONS LORS DE L\'ELABORATION D" +
    "ES QCMS";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1035, 554);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(222, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Copyright © 2016 Ciprel | Tous droits réservés";
            // 
            // addErrorGameQuestion
            // 
            this.addErrorGameQuestion.Location = new System.Drawing.Point(12, 453);
            this.addErrorGameQuestion.Name = "addErrorGameQuestion";
            this.addErrorGameQuestion.Size = new System.Drawing.Size(257, 35);
            this.addErrorGameQuestion.TabIndex = 10;
            this.addErrorGameQuestion.Text = "Ajouter une question \'Jeu des erreurs\'";
            this.addErrorGameQuestion.UseVisualStyleBackColor = true;
            this.addErrorGameQuestion.Click += new System.EventHandler(this.addErrorGameQuestion_Click);
            // 
            // addNormalQuestion
            // 
            this.addNormalQuestion.Location = new System.Drawing.Point(12, 412);
            this.addNormalQuestion.Name = "addNormalQuestion";
            this.addNormalQuestion.Size = new System.Drawing.Size(257, 35);
            this.addNormalQuestion.TabIndex = 11;
            this.addNormalQuestion.Text = "Ajouter une question";
            this.addNormalQuestion.UseVisualStyleBackColor = true;
            this.addNormalQuestion.Click += new System.EventHandler(this.addNormalQuestion_Click);
            // 
            // saveSuccessLabel
            // 
            this.saveSuccessLabel.AutoSize = true;
            this.saveSuccessLabel.ForeColor = System.Drawing.Color.ForestGreen;
            this.saveSuccessLabel.Location = new System.Drawing.Point(12, 503);
            this.saveSuccessLabel.Name = "saveSuccessLabel";
            this.saveSuccessLabel.Size = new System.Drawing.Size(119, 13);
            this.saveSuccessLabel.TabIndex = 12;
            this.saveSuccessLabel.Text = "Enregistrement effectué";
            this.saveSuccessLabel.Visible = false;
            // 
            // timer1
            // 
            this.timer1.Interval = 3000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // idDataGridViewTextBoxColumn2
            // 
            this.idDataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.idDataGridViewTextBoxColumn2.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn2.HeaderText = "Identifiant";
            this.idDataGridViewTextBoxColumn2.Name = "idDataGridViewTextBoxColumn2";
            this.idDataGridViewTextBoxColumn2.ReadOnly = true;
            this.idDataGridViewTextBoxColumn2.Visible = false;
            this.idDataGridViewTextBoxColumn2.Width = 60;
            // 
            // questionIdDataGridViewTextBoxColumn
            // 
            this.questionIdDataGridViewTextBoxColumn.DataPropertyName = "questionId";
            this.questionIdDataGridViewTextBoxColumn.HeaderText = "Identifiant de la question*";
            this.questionIdDataGridViewTextBoxColumn.Name = "questionIdDataGridViewTextBoxColumn";
            // 
            // contentDataGridViewTextBoxColumn1
            // 
            this.contentDataGridViewTextBoxColumn1.DataPropertyName = "content";
            this.contentDataGridViewTextBoxColumn1.HeaderText = "Réponse*";
            this.contentDataGridViewTextBoxColumn1.Name = "contentDataGridViewTextBoxColumn1";
            this.contentDataGridViewTextBoxColumn1.Width = 175;
            // 
            // correctAnswerDataGridViewCheckBoxColumn
            // 
            this.correctAnswerDataGridViewCheckBoxColumn.DataPropertyName = "correctAnswer";
            this.correctAnswerDataGridViewCheckBoxColumn.HeaderText = "Bonne réponse?";
            this.correctAnswerDataGridViewCheckBoxColumn.Name = "correctAnswerDataGridViewCheckBoxColumn";
            // 
            // DeleteAnswer
            // 
            this.DeleteAnswer.HeaderText = "Supprimer la répone";
            this.DeleteAnswer.Name = "DeleteAnswer";
            this.DeleteAnswer.Text = "Delete";
            this.DeleteAnswer.UseColumnTextForButtonValue = true;
            this.DeleteAnswer.Width = 70;
            // 
            // ManagerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1269, 576);
            this.Controls.Add(this.saveSuccessLabel);
            this.Controls.Add(this.addNormalQuestion);
            this.Controls.Add(this.addErrorGameQuestion);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.mainMenuButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.answerDataGrid);
            this.Controls.Add(this.questionDataGrid);
            this.Controls.Add(this.categoryDataGrid);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ManagerForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gestionnaire des QCMs";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ManageCategoriesForm_Closing);
            this.Load += new System.EventHandler(this.ManagerForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.qCMCiprelProjectDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qCMCiprelProjectDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.answerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoryDataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.questionDataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.questionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.answerDataGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private QCMCiprelProjectDataSet qCMCiprelProjectDataSet;
        private System.Windows.Forms.BindingSource qCMCiprelProjectDataSetBindingSource;
        private System.Windows.Forms.BindingSource answerBindingSource;
        private QCMCiprelProjectDataSetTableAdapters.AnswerTableAdapter answerTableAdapter;
        private System.Windows.Forms.BindingSource categoryBindingSource;
        private QCMCiprelProjectDataSetTableAdapters.CategoryTableAdapter categoryTableAdapter;
        private System.Windows.Forms.DataGridView categoryDataGrid;
        private System.Windows.Forms.DataGridView questionDataGrid;
        private System.Windows.Forms.BindingSource questionBindingSource;
        private QCMCiprelProjectDataSetTableAdapters.QuestionTableAdapter questionTableAdapter;
        private System.Windows.Forms.DataGridView answerDataGrid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button mainMenuButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button addErrorGameQuestion;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewButtonColumn DeleteCategory;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn contentDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn categoryIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn levelDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewImageColumn image;
        private System.Windows.Forms.DataGridViewTextBoxColumn commentQuestionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn commentCorrectAnswerDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn commentIncorrectAnswerDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewButtonColumn addImage;
        private System.Windows.Forms.DataGridViewButtonColumn removeImage;
        private System.Windows.Forms.DataGridViewButtonColumn DeleteQuestion;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isErrorGame;
        private System.Windows.Forms.Button addNormalQuestion;
        private System.Windows.Forms.Label saveSuccessLabel;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn questionIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn contentDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn correctAnswerDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewButtonColumn DeleteAnswer;
    }
}