﻿using QCMCiprelProject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QCMCiprelProjectAdmin
{
    /// <summary>
    ///     Permet d'ajouter une nouvelle question
    ///     // Used to add a new question
    /// </summary>
    public partial class AddNormalQuestionForm : Form
    {
        /// <summary>
        ///     Utilisé pour rappeler la fonction de mise à jour des tableaux après l'ajout
        ///     // Used to call the tab's update function after adding the question
        /// </summary>
        ManagerForm managerForm;

        /// <summary>
        ///     Constructeur par défaut
        ///     // Default constructor
        /// </summary>
        /// <param name="managerForm"></param>
        public AddNormalQuestionForm(ManagerForm managerForm)
        {
            InitializeComponent();
            this.managerForm = managerForm;
        }

        /// <summary>
        ///     Manager d'évènement, est appelé lors d'un clic dans une cellue
        ///     // Event handler, call on clic on a cell
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex].Name == "delete")
                if (e.RowIndex != dataGridView1.Rows.Count - 1)
                    dataGridView1.Rows.RemoveAt(e.RowIndex);
        }

        /// <summary>
        ///     Initialisation du formulaire
        ///     // Form initialisation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddNormalQuestionForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'qCMCiprelProjectDataSet.Answer' table. You can move, or remove it, as needed.
            //this.answerTableAdapter.Fill(this.qCMCiprelProjectDataSet.Answer);
            // TODO: This line of code loads data into the 'qCMCiprelProjectDataSet.Category' table. You can move, or remove it, as needed.
            //this.categoryTableAdapter.Fill(this.qCMCiprelProjectDataSet.Category);

            Dictionary<int, string> quizzLevel = new Dictionary<int, string>();
            quizzLevel.Add(1, "Novice");
            quizzLevel.Add(2, "Intermédiaire");
            quizzLevel.Add(3, "Avancé");

            levelComboBox.DataSource = new BindingSource(quizzLevel, null);
            levelComboBox.DisplayMember = "Value";
            levelComboBox.ValueMember = "Key";

            Dictionary<long, string> categories = new Dictionary<long, string>();

            try
            {
                SqlConnection conn = new SqlConnection(Properties.Resources.DatabaseConnectionString2);

                conn.Open();
                List<Category> categoryList = new List<Category>();
                SqlCommand cmd = new SqlCommand(@"SELECT * FROM Category", conn);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                    categoryList.Add(new Category(reader.GetInt64(0), reader.GetString(1)));
                reader.Close();

                foreach (Category cat in categoryList) categories.Add(cat.id, cat.name);

                if (categories.Count > 0)
                {
                    categoriesComboBox.DataSource = new BindingSource(categories, null);
                    categoriesComboBox.DisplayMember = "Value";
                    categoriesComboBox.ValueMember = "Key";
                }

                conn.Close();

                pictureBox1.Image = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter au server", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.Write(ex);
                Close();
            }
        }

        /// <summary>
        ///     Bouton permettant d'ajouter une image à la question
        ///     // Button adding a picture to the question
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addEditPictureButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    pictureBox1.Image = Image.FromFile(openFileDialog.FileName);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        /// <summary>
        ///     Bouton permettant de supprimer l'image de la question
        ///     // Button deleting the question's picture
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void deletePictureButton_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = null;
            pictureBox1.Update();
        }

        /// <summary>
        ///     Ouvre un plus grand apperçu de l'image
        ///     // Opens a bigger view of the picture
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (pictureBox1.Image != null)
            {
                new PictureViewerForm(pictureBox1.Image).Show();
            }
        }

        /// <summary>
        ///     Retour à l'interface d'administration
        ///     // Back to the management interface
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        ///     Enregistre la question
        ///     // Saves the question
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveButton_Click(object sender, EventArgs e)
        {
            errorQuestion.Visible = errorAnswers.Visible = false;

            if (questionTextBox.Text.Length == 0)
            {
                errorQuestion.Visible = true;
                return;
            }

            bool containsGoodAnswer = false;
            if (dataGridView1.Rows.Count >= 3)
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    if (row.Cells[1].Value == null || row.Cells[1].Value.Equals("")) row.Cells[1].Value = false;
                    if ((bool)row.Cells[1].Value) containsGoodAnswer = true;
                }
            if (!containsGoodAnswer)
            {
                errorAnswers.Visible = true;
                return;
            }

            try
            {
                SqlConnection conn = new SqlConnection(Properties.Resources.DatabaseConnectionString2);
                SqlCommand command;

                if (pictureBox1.Image != null)
                    command = new SqlCommand(@"INSERT INTO " +
                    "Question (content, commentQuestion, commentCorrectAnswer, commentIncorrectAnswer, categoryId, " +
                    "level, isErrorGame, image) OUTPUT INSERTED.id VALUES (@content, @commentQuestion, " +
                    "@commentCorrectAnswer, @commentIncorrectAnswer, @catId, @level, 0, @image)", conn);
                else
                    command = new SqlCommand(@"INSERT INTO " +
                     "Question (content, commentQuestion, commentCorrectAnswer, commentIncorrectAnswer, categoryId, " +
                     "level, isErrorGame) OUTPUT INSERTED.id VALUES (@content, @commentQuestion, " +
                     "@commentCorrectAnswer, @commentIncorrectAnswer, @catId, @level, 0)", conn);

                command.Parameters.AddWithValue("@content", questionTextBox.Text);
                command.Parameters.AddWithValue("@commentQuestion", commentQuestionTextBox.Text);
                command.Parameters.AddWithValue("@commentCorrectAnswer", commentCorrectAnswerTextBox.Text);
                command.Parameters.AddWithValue("@commentIncorrectAnswer", commentIncorrectAnswerTextBox.Text);
                command.Parameters.AddWithValue("@catId", ((KeyValuePair<long, string>)categoriesComboBox.SelectedItem).Key);
                command.Parameters.AddWithValue("@level", ((KeyValuePair<int, string>)levelComboBox.SelectedItem).Key);
                if (pictureBox1.Image != null)
                    command.Parameters.AddWithValue("@image", (byte[])new ImageConverter().ConvertTo(pictureBox1.Image, typeof(byte[])));

                conn.Open();
                long questionId = (long)command.ExecuteScalar();
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    if (row.Cells[0].Value != null && !row.Cells[0].Value.Equals(""))
                    {
                        command = new SqlCommand(@"INSERT INTO Answer (questionId, content, correctAnswer) " +
                        "VALUES (@questionId, @content, @correctAnswer)", conn);
                        command.Parameters.AddWithValue("@questionId", questionId);
                        command.Parameters.AddWithValue("@content", row.Cells[0].Value);
                        command.Parameters.AddWithValue("@correctAnswer", (bool)row.Cells[1].Value);
                        command.ExecuteNonQuery();
                    }
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter au server", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.Out.Write(ex);
            }
            managerForm.ManagerForm_Load(sender, e);
            Close();
        }
    }
}
