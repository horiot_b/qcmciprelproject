﻿namespace QCMCiprelProjectAdmin
{
    partial class AddNormalQuestionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddNormalQuestionForm));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.categoriesComboBox = new System.Windows.Forms.ComboBox();
            this.categoryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.qCMCiprelProjectDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.qCMCiprelProjectDataSet = new QCMCiprelProjectAdmin.QCMCiprelProjectDataSet();
            this.levelComboBox = new System.Windows.Forms.ComboBox();
            this.questionTextBox = new System.Windows.Forms.TextBox();
            this.commentQuestionTextBox = new System.Windows.Forms.TextBox();
            this.commentCorrectAnswerTextBox = new System.Windows.Forms.TextBox();
            this.commentIncorrectAnswerTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.addEditPictureButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.deletePictureButton = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.categoryTableAdapter = new QCMCiprelProjectAdmin.QCMCiprelProjectDataSetTableAdapters.CategoryTableAdapter();
            this.errorAnswers = new System.Windows.Forms.Label();
            this.errorQuestion = new System.Windows.Forms.Label();
            this.answerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.answerTableAdapter = new QCMCiprelProjectAdmin.QCMCiprelProjectDataSetTableAdapters.AnswerTableAdapter();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.answer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.goodAnswer = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.delete = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.categoryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qCMCiprelProjectDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qCMCiprelProjectDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.answerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Question*";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Catégorie*";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(378, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Niveau*";
            // 
            // categoriesComboBox
            // 
            this.categoriesComboBox.DataSource = this.categoryBindingSource;
            this.categoriesComboBox.DisplayMember = "name";
            this.categoriesComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.categoriesComboBox.FormattingEnabled = true;
            this.categoriesComboBox.Location = new System.Drawing.Point(12, 29);
            this.categoriesComboBox.Name = "categoriesComboBox";
            this.categoriesComboBox.Size = new System.Drawing.Size(362, 21);
            this.categoriesComboBox.TabIndex = 1;
            // 
            // categoryBindingSource
            // 
            this.categoryBindingSource.DataMember = "Category";
            this.categoryBindingSource.DataSource = this.qCMCiprelProjectDataSetBindingSource;
            // 
            // qCMCiprelProjectDataSetBindingSource
            // 
            this.qCMCiprelProjectDataSetBindingSource.DataSource = this.qCMCiprelProjectDataSet;
            this.qCMCiprelProjectDataSetBindingSource.Position = 0;
            // 
            // qCMCiprelProjectDataSet
            // 
            this.qCMCiprelProjectDataSet.DataSetName = "QCMCiprelProjectDataSet";
            this.qCMCiprelProjectDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // levelComboBox
            // 
            this.levelComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.levelComboBox.FormattingEnabled = true;
            this.levelComboBox.Location = new System.Drawing.Point(380, 29);
            this.levelComboBox.Name = "levelComboBox";
            this.levelComboBox.Size = new System.Drawing.Size(350, 21);
            this.levelComboBox.TabIndex = 2;
            // 
            // questionTextBox
            // 
            this.questionTextBox.Location = new System.Drawing.Point(12, 80);
            this.questionTextBox.Name = "questionTextBox";
            this.questionTextBox.Size = new System.Drawing.Size(718, 20);
            this.questionTextBox.TabIndex = 3;
            // 
            // commentQuestionTextBox
            // 
            this.commentQuestionTextBox.Location = new System.Drawing.Point(12, 137);
            this.commentQuestionTextBox.Name = "commentQuestionTextBox";
            this.commentQuestionTextBox.Size = new System.Drawing.Size(362, 20);
            this.commentQuestionTextBox.TabIndex = 4;
            // 
            // commentCorrectAnswerTextBox
            // 
            this.commentCorrectAnswerTextBox.Location = new System.Drawing.Point(12, 195);
            this.commentCorrectAnswerTextBox.Name = "commentCorrectAnswerTextBox";
            this.commentCorrectAnswerTextBox.Size = new System.Drawing.Size(362, 20);
            this.commentCorrectAnswerTextBox.TabIndex = 5;
            // 
            // commentIncorrectAnswerTextBox
            // 
            this.commentIncorrectAnswerTextBox.Location = new System.Drawing.Point(12, 254);
            this.commentIncorrectAnswerTextBox.Name = "commentIncorrectAnswerTextBox";
            this.commentIncorrectAnswerTextBox.Size = new System.Drawing.Size(362, 20);
            this.commentIncorrectAnswerTextBox.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 118);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(191, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Commentaire affiché lors de la question";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 179);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(224, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Commentaire affiché si la réponse est correcte";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 238);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(232, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Commentaire affiché si la réponse est incorrecte";
            // 
            // addEditPictureButton
            // 
            this.addEditPictureButton.Location = new System.Drawing.Point(12, 287);
            this.addEditPictureButton.Name = "addEditPictureButton";
            this.addEditPictureButton.Size = new System.Drawing.Size(133, 23);
            this.addEditPictureButton.TabIndex = 7;
            this.addEditPictureButton.Text = "Ajouter / Editer l\'image";
            this.addEditPictureButton.UseVisualStyleBackColor = true;
            this.addEditPictureButton.Click += new System.EventHandler(this.addEditPictureButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(559, 412);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(171, 59);
            this.saveButton.TabIndex = 11;
            this.saveButton.Text = "Enregistrer";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(380, 412);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(171, 59);
            this.cancelButton.TabIndex = 10;
            this.cancelButton.Text = "Annuler";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(12, 317);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(273, 154);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // deletePictureButton
            // 
            this.deletePictureButton.Location = new System.Drawing.Point(151, 287);
            this.deletePictureButton.Name = "deletePictureButton";
            this.deletePictureButton.Size = new System.Drawing.Size(134, 23);
            this.deletePictureButton.TabIndex = 8;
            this.deletePictureButton.Text = "Supprimer l\'image";
            this.deletePictureButton.UseVisualStyleBackColor = true;
            this.deletePictureButton.Click += new System.EventHandler(this.deletePictureButton_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(380, 118);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "Réponses";
            // 
            // categoryTableAdapter
            // 
            this.categoryTableAdapter.ClearBeforeFill = true;
            // 
            // errorAnswers
            // 
            this.errorAnswers.AutoSize = true;
            this.errorAnswers.ForeColor = System.Drawing.Color.DarkRed;
            this.errorAnswers.Location = new System.Drawing.Point(442, 118);
            this.errorAnswers.Name = "errorAnswers";
            this.errorAnswers.Size = new System.Drawing.Size(283, 13);
            this.errorAnswers.TabIndex = 19;
            this.errorAnswers.Text = "Veuillez donner au moins deux réponses dont une correcte";
            this.errorAnswers.Visible = false;
            // 
            // errorQuestion
            // 
            this.errorQuestion.AutoSize = true;
            this.errorQuestion.ForeColor = System.Drawing.Color.DarkRed;
            this.errorQuestion.Location = new System.Drawing.Point(69, 64);
            this.errorQuestion.Name = "errorQuestion";
            this.errorQuestion.Size = new System.Drawing.Size(149, 13);
            this.errorQuestion.TabIndex = 20;
            this.errorQuestion.Text = "Veuillez renseigner la question";
            this.errorQuestion.Visible = false;
            // 
            // answerBindingSource
            // 
            this.answerBindingSource.DataMember = "Answer";
            this.answerBindingSource.DataSource = this.qCMCiprelProjectDataSetBindingSource;
            // 
            // answerTableAdapter
            // 
            this.answerTableAdapter.ClearBeforeFill = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.answer,
            this.goodAnswer,
            this.delete});
            this.dataGridView1.Location = new System.Drawing.Point(383, 137);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(347, 269);
            this.dataGridView1.TabIndex = 9;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // answer
            // 
            this.answer.HeaderText = "Réponse";
            this.answer.Name = "answer";
            // 
            // goodAnswer
            // 
            this.goodAnswer.HeaderText = "Bonne réponse?";
            this.goodAnswer.Name = "goodAnswer";
            // 
            // delete
            // 
            this.delete.HeaderText = "Supprimer";
            this.delete.Name = "delete";
            // 
            // AddNormalQuestionForm
            // 
            this.AcceptButton = this.saveButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(742, 483);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.errorQuestion);
            this.Controls.Add(this.errorAnswers);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.deletePictureButton);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.addEditPictureButton);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.commentIncorrectAnswerTextBox);
            this.Controls.Add(this.commentCorrectAnswerTextBox);
            this.Controls.Add(this.commentQuestionTextBox);
            this.Controls.Add(this.questionTextBox);
            this.Controls.Add(this.levelComboBox);
            this.Controls.Add(this.categoriesComboBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AddNormalQuestionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ajouter une question";
            this.Load += new System.EventHandler(this.AddNormalQuestionForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.categoryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qCMCiprelProjectDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qCMCiprelProjectDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.answerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox categoriesComboBox;
        private System.Windows.Forms.ComboBox levelComboBox;
        private System.Windows.Forms.TextBox questionTextBox;
        private System.Windows.Forms.TextBox commentQuestionTextBox;
        private System.Windows.Forms.TextBox commentCorrectAnswerTextBox;
        private System.Windows.Forms.TextBox commentIncorrectAnswerTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button addEditPictureButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button deletePictureButton;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.BindingSource qCMCiprelProjectDataSetBindingSource;
        private QCMCiprelProjectDataSet qCMCiprelProjectDataSet;
        private System.Windows.Forms.BindingSource categoryBindingSource;
        private QCMCiprelProjectDataSetTableAdapters.CategoryTableAdapter categoryTableAdapter;
        private System.Windows.Forms.Label errorAnswers;
        private System.Windows.Forms.Label errorQuestion;
        private System.Windows.Forms.BindingSource answerBindingSource;
        private QCMCiprelProjectDataSetTableAdapters.AnswerTableAdapter answerTableAdapter;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn answer;
        private System.Windows.Forms.DataGridViewCheckBoxColumn goodAnswer;
        private System.Windows.Forms.DataGridViewButtonColumn delete;
    }
}