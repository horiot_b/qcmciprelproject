﻿using QCMCiprelProject;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Resources;
using System.Windows.Forms;

namespace QCMCiprelProjectAdmin
{
    /// <summary>
    ///     Interface d'administration // Management interface
    /// </summary>
    public partial class ManagerForm : Form
    {
        private InitForm initForm;

        // Objets nécessaires pour les requêtes vers la base de données // Mandatory objects to do the database's requests
        private DataTable categoryTable = new DataTable();
        private DataTable questionTable = new DataTable();
        private DataTable answerTable = new DataTable();
        private SqlDataAdapter categoryDA;
        private SqlDataAdapter questionDA;
        private SqlDataAdapter answerDA;
        private SqlCommandBuilder sqlCommandBuilder = null;
        private SqlConnection conn;

        /// <summary>
        ///     Constructeur avec paramètres // Constructor with parameters
        /// </summary>
        /// <param name="initForm"></param>
        public ManagerForm(InitForm initForm)
        {
            InitializeComponent();
            this.initForm = initForm;
        }

        /// <summary>
        ///     Liste toutes les données de la base // Displays all the database's data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ManagerForm_Load(object sender, EventArgs e)
        {
            categoryDataGrid.DataSource = null;
            categoryTable.Clear();
            questionTable.Clear();
            answerTable.Clear();

            String categoryQueryString = "SELECT * FROM Category";
            String questionQueryString = "SELECT * FROM Question";
            String answerQueryString = "SELECT * FROM Answer";

            try
            {
                conn = new SqlConnection(Properties.Resources.DatabaseConnectionString2);
                conn.Open();
                SqlCommand command = conn.CreateCommand();

                command.CommandText = categoryQueryString;
                categoryDA = new SqlDataAdapter(categoryQueryString, conn);
                sqlCommandBuilder = new SqlCommandBuilder(categoryDA);
                categoryDA.Fill(categoryTable);
                categoryDataGrid.DataSource = new BindingSource { DataSource = categoryTable };

                command.CommandText = questionQueryString;
                questionDA = new SqlDataAdapter(questionQueryString, conn);
                sqlCommandBuilder = new SqlCommandBuilder(questionDA);
                questionDA.Fill(questionTable);
                questionDataGrid.DataSource = new BindingSource { DataSource = questionTable };
                questionTable.Columns["isErrorGame"].DefaultValue = false;

                command.CommandText = answerQueryString;
                answerDA = new SqlDataAdapter(answerQueryString, conn);
                sqlCommandBuilder = new SqlCommandBuilder(answerDA);
                answerDA.Fill(answerTable);
                answerDataGrid.DataSource = new BindingSource { DataSource = answerTable };
                answerTable.Columns["correctAnswer"].DefaultValue = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter au server", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.Write(ex);
            }
        }

        /// <summary>
        ///     Enregistre les changements dans la pase de données // Saves the changes in the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveButton_Click(object sender, EventArgs e)
        {
            categoryDataGrid.EndEdit();
            questionDataGrid.EndEdit();
            answerDataGrid.EndEdit();

            try
            {
                categoryDA.Update(categoryTable);
                questionDA.Update(questionTable);
                answerDA.Update(answerTable);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Vérifiez que les questions ai des identifiants de catégorie corrects et que " +
                    "les réponses aient des identifiants de question corrects\n\nSi vous souhaitez supprimer un élément, " +
                    "les informations se trouvent en bas à gauche de la fenêtre d'édition");
                Console.WriteLine(ex);
            }
            saveSuccessLabel.Visible = true;
            timer1.Start();
            ManagerForm_Load(sender, e);
        }

        /// <summary>
        ///     Retourne au menu principal // Returns to the main menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mainMenuButton_Click(object sender, EventArgs e)
        {
            conn.Close();
            initForm.Show();
            initForm.Init_Load(sender, e);
            Close();
        }

        /// <summary>
        ///     Gestionnaire d'évènements, est appelé lors d'un clic dans une cellule du tableau des categories
        ///     // Event handler, is called when the users clics on a cell in the categorie's tab
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void categoryDataGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != categoryDataGrid.Rows.Count - 1)
                if (categoryDataGrid.Columns[e.ColumnIndex].Name == "DeleteCategory")
                {
                    var confirmResult = MessageBox.Show("Confirmer la suppression de l'élément?",
                                         "Confirmation requise",
                                         MessageBoxButtons.YesNo);
                    if (confirmResult == DialogResult.Yes)
                    {
                        try
                        {
                            SqlConnection conn = new SqlConnection(Properties.Resources.DatabaseConnectionString2);
                            SqlDataAdapter da = new SqlDataAdapter();
                            DataSet ds = new DataSet();
                            DataTable dt = new DataTable();

                            List<long> questionIdList = new List<long>();

                            conn.Open();
                            SqlCommand cmd = new SqlCommand(@"SELECT id FROM Question WHERE categoryId=" +
                            categoryDataGrid.Rows[e.RowIndex].Cells[0].Value, conn);
                            SqlDataReader reader = cmd.ExecuteReader();
                            while (reader.Read())
                                questionIdList.Add(reader.GetInt64(0));
                            reader.Close();

                            foreach (long questionId in questionIdList)
                            {
                                new SqlCommand(@"DELETE FROM Answer WHERE questionId=" + questionId, conn).ExecuteNonQuery();
                                new SqlCommand(@"DELETE FROM Question WHERE id=" + questionId, conn).ExecuteNonQuery();
                            }

                            new SqlCommand(@"DELETE FROM Category WHERE id=" + categoryDataGrid.Rows[e.RowIndex].Cells[0].Value, conn).ExecuteNonQuery();

                            conn.Close();

                            ManagerForm_Load(sender, e);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Impossible de se connecter au server", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            Console.Write(ex);
                        }
                    }
                }
        }

        /// <summary>
        ///     Gestionnaire d'évènements, est appelé lors d'un clic dans une cellule du tableau des questions
        ///     // Event handler, is called when the users clics on a cell in the question's tab
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void questionDataGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != questionDataGrid.Rows.Count - 1)
                switch (questionDataGrid.Columns[e.ColumnIndex].Name)
                {
                    case "addImage":
                        OpenFileDialog openFileDialog1 = new OpenFileDialog();
                        openFileDialog1.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
                        openFileDialog1.RestoreDirectory = true;

                        if (openFileDialog1.ShowDialog() == DialogResult.OK)
                        {
                            try
                            {
                                DataGridViewImageCell cell = (DataGridViewImageCell)questionDataGrid.Rows[e.RowIndex].Cells[4];
                                cell.Value = Image.FromFile(openFileDialog1.FileName);
                                questionDataGrid.InvalidateCell(cell);
                                questionDataGrid.Update();
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                            }
                        }
                        break;

                    case "image":
                        if (questionDataGrid.Rows[e.RowIndex].Cells[4].Value != DBNull.Value)
                            new PictureViewerForm(Image.FromStream(new MemoryStream((byte[])questionDataGrid.Rows[e.RowIndex].Cells[4].Value))).Show();
                        break;

                    case "removeImage":
                        DataGridViewImageCell cell2 = (DataGridViewImageCell)questionDataGrid.Rows[e.RowIndex].Cells[4];

                        cell2.Value = null;
                        questionDataGrid.InvalidateCell(cell2);
                        questionDataGrid.Update();
                        break;

                    case "DeleteQuestion":
                        var confirmResult = MessageBox.Show("Confirmer la suppression de l'élément?",
                                         "Confirmation requise",
                                         MessageBoxButtons.YesNo);
                        if (confirmResult == DialogResult.Yes)
                        {
                            try
                            {
                                SqlConnection conn = new SqlConnection(Properties.Resources.DatabaseConnectionString2);

                                conn.Open();
                                new SqlCommand(@"DELETE FROM Answer WHERE questionId=" +
                                    questionDataGrid.Rows[e.RowIndex].Cells[0].Value, conn).ExecuteNonQuery();
                                new SqlCommand(@"DELETE FROM Question WHERE id=" +
                                    questionDataGrid.Rows[e.RowIndex].Cells[0].Value, conn).ExecuteNonQuery();
                                conn.Close();

                                ManagerForm_Load(sender, e);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("Impossible de se connecter au server", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                Console.Write(ex);
                            }
                        }
                        break;
                }
        }

        /// <summary>
        ///     Gestionnaire d'évènement, est appelé à la fermeture de la fenêtre
        ///     // Event handler, is called when the window is closing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ManageCategoriesForm_Closing(object sender, EventArgs e)
        {
            conn.Close();
            initForm.Show();
            initForm.Init_Load(sender, e);
        }

        /// <summary>
        ///     Bouton permettant d'ouvrir le forulaire d'ajout de question pour le jeu des erreurs
        ///     // Button that opens the form allowing the insertion of a new question of type error game
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addErrorGameQuestion_Click(object sender, EventArgs e)
        {
            new AddErrorGameQuestionForm(this).Show();
        }

        /// <summary>
        ///     Gestionnaire d'évènements, est appelé lors d'un clic dans une cellule du tableau des questions
        ///     // Event handler, is called when the users clics on a cell in the question's tab
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void answerDataGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != answerDataGrid.Rows.Count - 1)
                if (answerDataGrid.Columns[e.ColumnIndex].Name == "DeleteAnswer")
                {
                    var confirmResult = MessageBox.Show("Confirmer la suppression de l'élément?",
                                         "Confirmation requise",
                                         MessageBoxButtons.YesNo);
                    if (confirmResult == DialogResult.Yes)
                    {
                        try
                        {
                            SqlConnection conn = new SqlConnection(Properties.Resources.DatabaseConnectionString2);
                            conn.Open();
                            new SqlCommand(@"DELETE FROM Answer WHERE Id=" + answerDataGrid.Rows[e.RowIndex].Cells[0].Value, conn).ExecuteNonQuery();
                            conn.Close();
                            ManagerForm_Load(sender, e);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Impossible de se connecter au server", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            Console.Write(ex);
                        }
                    }
                }
        }

        /// <summary>
        ///     Bouton permettant d'ouvrir le forulaire d'ajout de question
        ///     // Button that opens the form allowing the insertion of a new question
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addNormalQuestion_Click(object sender, EventArgs e)
        {
            new AddNormalQuestionForm(this).Show();
        }

        /// <summary>
        ///     Chronomètre ayant pour but de faire disparaitre le texte "Enregistrement effectué" au bout de 3 sec
        ///     // Timer used to hide the text "Enregistrement effectué" after 3 sec
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer1_Tick(object sender, EventArgs e)
        {
            saveSuccessLabel.Visible = false;
            timer1.Stop();
        }
    }
}
