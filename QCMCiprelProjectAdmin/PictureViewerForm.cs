﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QCMCiprelProjectAdmin
{
    public partial class PictureViewerForm : Form
    {
        public PictureViewerForm(Image image)
        {
            InitializeComponent();
            pictureBox1.Image = image;
            
            if (image.Height > (Screen.FromControl(this).Bounds.Height - 50))
            {
                float scale = (float)(Screen.FromControl(this).Bounds.Height - 50) / image.Height;
                float newX = image.Size.Width * scale;
                float newY = image.Size.Height * scale;
                pictureBox1.Size = new Size((int)newX, (int)newY);
            }
            else if (image.Width > (Screen.FromControl(this).Bounds.Width - 50))
            {
                float scale = (float)Screen.FromControl(this).Bounds.Width / image.Width;
                float newX = image.Size.Width * scale;
                float newY = image.Size.Height * scale;
                pictureBox1.Size = new Size((int)newX, (int)newY);
            }

            Size = pictureBox1.Size;
        }

        private void PictureViewerForm_Load(object sender, EventArgs e)
        {

        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}
