﻿using QCMCiprelProject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QCMCiprelProjectAdmin
{
    public partial class AddErrorGameQuestionForm : Form
    {
        Rectangle currRect;
        Point endPoint;
        bool isDrag;
        Point startPoint;
        Image image;
        Graphics graphics;

        ManagerForm managerForm;

        List<Rectangle> answers = new List<Rectangle>();
        List<Rectangle> coords = new List<Rectangle>();

        public AddErrorGameQuestionForm(ManagerForm managerForm)
        {
            InitializeComponent();
            this.managerForm = managerForm;
            graphics = pictureBox1.CreateGraphics();
        }

        private void AddErrorGameQuestionForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'qCMCiprelProjectDataSet.Category' table. You can move, or remove it, as needed.
            //this.categoryTableAdapter.Fill(this.qCMCiprelProjectDataSet.Category);

            Dictionary<int, string> quizzLevel = new Dictionary<int, string>();
            quizzLevel.Add(1, "Novice");
            quizzLevel.Add(2, "Intermédiaire");
            quizzLevel.Add(3, "Avancé");

            levelComboBox.DataSource = new BindingSource(quizzLevel, null);
            levelComboBox.DisplayMember = "Value";
            levelComboBox.ValueMember = "Key";

            Dictionary<long, string> categories = new Dictionary<long, string>();

            try
            {
                SqlConnection conn = new SqlConnection(Properties.Resources.DatabaseConnectionString2);

                conn.Open();
                List<Category> categoryList = new List<Category>();
                SqlCommand cmd = new SqlCommand(@"SELECT * FROM Category", conn);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                    categoryList.Add(new Category(reader.GetInt64(0), reader.GetString(1)));
                reader.Close();

                foreach (Category cat in categoryList) categories.Add(cat.id, cat.name);

                if (categories.Count > 0)
                {
                    categoriesComboBox.DataSource = new BindingSource(categories, null);
                    categoriesComboBox.DisplayMember = "Value";
                    categoriesComboBox.ValueMember = "Key";
                }

                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter au server", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.Write(ex);
                Close();
            }
        }

        private void addEditPictureButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    pictureBox1.Image = Image.FromFile(openFileDialog.FileName);
                    image = pictureBox1.Image;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (pictureBox1.Image == null)
            {
                errorMessage.Text = "L'image et au moins une réponse sont oligatoires, cliquez sur le bouton 'Aide' si besoin";
                errorMessage.Visible = true;
                return;
            }
            if (questionContent.Text.Length == 0)
            {
                errorMessage.Text = "La question est obligatoire";
                errorMessage.Visible = true;
                return;
            }
            if (answers.Count == 0)
            {
                errorMessage.Text = "Au moins une réponse est oligatoire, cliquez sur le bouton 'Aide' si besoin";
                errorMessage.Visible = true;
                return;
            }
            try
            {
                SqlConnection conn = new SqlConnection(Properties.Resources.DatabaseConnectionString2);
                SqlCommand command = new SqlCommand(@"INSERT INTO " + 
                    "Question (content, commentQuestion, categoryId, level, isErrorGame, image) " +
                    "OUTPUT INSERTED.id VALUES (@content, @comment, @catId, @level, 1, @image)", conn);

                command.Parameters.AddWithValue("@content", questionContent.Text);
                command.Parameters.AddWithValue("@comment", questionComment.Text);
                command.Parameters.AddWithValue("@catId", ((KeyValuePair<long, string>)categoriesComboBox.SelectedItem).Key);
                command.Parameters.AddWithValue("@level", ((KeyValuePair<int, string>)levelComboBox.SelectedItem).Key);
                command.Parameters.AddWithValue("@image", (byte[])new ImageConverter().ConvertTo(pictureBox1.Image, typeof(byte[])));

                conn.Open();
                long questionId = (long)command.ExecuteScalar();
                foreach (Rectangle elem in answers)
                {
                    command = new SqlCommand(@"INSERT INTO Answer (questionId, content, correctAnswer) " +
                        "VALUES (@questionId, @content, 1)", conn);
                    command.Parameters.AddWithValue("@questionId", questionId);
                    command.Parameters.AddWithValue("@content", elem.X + ";" + elem.Y + ";" + elem.Width + ";" + elem.Height);
                    command.ExecuteNonQuery();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter au server", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.Out.Write(ex);
            }
            managerForm.ManagerForm_Load(sender, e);
            Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            MouseEventArgs me = (MouseEventArgs)e;
            Point coordinates = convertCoordinates(me.Location);
            Console.Out.Write("Coord : " + coordinates.X + " " + coordinates.Y + "\n");

            foreach (Rectangle rect in answers)
            {
                Console.Out.Write("Rect : " + rect.X + " " + rect.Y + " - " + rect.Width + " " + rect.Height + "\n");
                if (rect.Contains(coordinates))
                {
                    coords.RemoveAt(answers.IndexOf(rect));
                    answers.Remove(rect);
                    pictureBox1.Image = image;
                    Invalidate();
                    Update();

                    foreach (Rectangle tmp in coords)
                        graphics.DrawRectangle(new Pen(Brushes.LawnGreen, 3), tmp);

                    break;
                }

            }
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            startPoint = new Point(e.X, e.Y);
            if (e.Button == MouseButtons.Left)
            {
                currRect = new Rectangle();
                currRect.X = startPoint.X;
                currRect.Y = startPoint.Y;
                isDrag = true;
            }
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (isDrag)
            {
                endPoint = new Point(e.X, e.Y);
                currRect.X = Math.Min(startPoint.X, endPoint.X);
                currRect.Y = Math.Min(startPoint.Y, endPoint.Y);
                currRect.Width = Math.Abs(startPoint.X - endPoint.X);
                currRect.Height = Math.Abs(startPoint.Y - endPoint.Y);
            }
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (pictureBox1.Image != null && (currRect.Height > 50 || currRect.Width > 50))
            {
                isDrag = false;

                //check not out of image
                if (isOutOfFrame()) return;

                graphics.DrawRectangle(new Pen(Brushes.LawnGreen, 3), currRect);
                coords.Add(currRect);

                Point newLocation = convertCoordinates(new Point(currRect.X, currRect.Y));
                Point newSize = convertSize(new Point(currRect.Width, currRect.Height));

                currRect.X = newLocation.X;
                currRect.Y = newLocation.Y;
                currRect.Width = newSize.X;
                currRect.Height = newSize.Y;

                if (!answers.Contains(currRect))
                    answers.Add(currRect);
            }
        }

        private Point convertCoordinates(Point coordinates)
        {
            // test to make sure our image is not null
            if (pictureBox1.Image == null) return coordinates;
            // Make sure our control width and height are not 0 and our 
            // image width and height are not 0
            if (pictureBox1.Width == 0 || pictureBox1.Height == 0 ||
                pictureBox1.Image.Width == 0 || pictureBox1.Image.Height == 0) return coordinates;
            // This is the one that gets a little tricky. Essentially, need to check 
            // the aspect ratio of the image to the aspect ratio of the control
            // to determine how it is being rendered
            float imageAspect = (float)pictureBox1.Image.Width / pictureBox1.Image.Height;
            float controlAspect = (float)pictureBox1.Width / pictureBox1.Height;
            float newX = coordinates.X;
            float newY = coordinates.Y;
            if (imageAspect > controlAspect)
            {
                // This means that we are limited by width, 
                // meaning the image fills up the entire control from left to right
                float ratioWidth = (float)pictureBox1.Image.Width / pictureBox1.Width;
                newX *= ratioWidth;
                float scale = (float)Width / pictureBox1.Image.Width;
                float displayHeight = scale * pictureBox1.Image.Height;
                float diffHeight = pictureBox1.Height - displayHeight;
                diffHeight /= 2;
                newY -= diffHeight;
                newY /= scale;
            }
            else
            {
                // This means that we are limited by height, 
                // meaning the image fills up the entire control from top to bottom
                float ratioHeight = (float)pictureBox1.Image.Height / pictureBox1.Height;
                newY *= ratioHeight;
                float scale = (float)pictureBox1.Height / pictureBox1.Image.Height;
                float displayWidth = scale * pictureBox1.Image.Width;
                float diffWidth = pictureBox1.Width - displayWidth;
                diffWidth /= 2;
                newX -= diffWidth;
                newX /= scale;
            }
            return new Point((int)newX, (int)newY);
        }

        private Point convertSize(Point coordinates)
        {
            if (pictureBox1.Image == null) return coordinates;
            if (pictureBox1.Width == 0 || pictureBox1.Height == 0 ||
                pictureBox1.Image.Width == 0 || pictureBox1.Image.Height == 0) return coordinates;
            float imageAspect = (float)pictureBox1.Image.Width / pictureBox1.Image.Height;
            float controlAspect = (float)pictureBox1.Width / pictureBox1.Height;
            float scale;
            if (imageAspect > controlAspect)
                scale = (float)pictureBox1.Width / pictureBox1.Image.Width;
            else
                scale = (float)pictureBox1.Height / pictureBox1.Image.Height;
            return new Point((int)(coordinates.X / scale), (int)(coordinates.Y / scale));
        }

        private bool isOutOfFrame()
        {
            float imageAspect = (float)pictureBox1.Image.Width / pictureBox1.Image.Height;
            float controlAspect = (float)pictureBox1.Width / pictureBox1.Height;
            if (imageAspect > controlAspect)
            {
                float scale = (float)pictureBox1.Width / pictureBox1.Image.Width;
                float displayHeight = scale * pictureBox1.Image.Height;
                float diffHeight = pictureBox1.Height - displayHeight;
                if (currRect.Y < diffHeight / 2 || currRect.Y > (diffHeight / 2) + displayHeight ||
                    currRect.Y + currRect.Height > (diffHeight / 2) + diffHeight)
                    return true;
            }
            else
            {

                float scale = (float)pictureBox1.Height / pictureBox1.Image.Height;
                float displayWidth = scale * pictureBox1.Image.Width;
                float diffWidth = pictureBox1.Width - displayWidth;
                if (currRect.X < diffWidth / 2 || currRect.X > (diffWidth / 2) + displayWidth ||
                    currRect.X + currRect.Width > (diffWidth / 2) + displayWidth)
                    return true;
            }
            return false;
        }

        private void helpButton_Click(object sender, EventArgs e)
        {
            new HelpAddErrorGameQuestionForm().Show();
        }
    }
}
