﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using QCMCiprelProject;

namespace QCMCiprelProjectAdmin
{
    /// <summary>
    ///     Menu principal // Main menu
    /// </summary>
    public partial class InitForm : Form
    {
        /// <summary>
        ///     Utilisé pour afficher le logo avec un fondu // Used to display the logo with a fade in effect
        /// </summary>
        private int opacity = 0;

        /// <summary>
        ///     Constructeur par défaut // Default constructor
        /// </summary>
        public InitForm()
        {
            InitializeComponent();
        }

        /// <summary>
        ///     Initialise les menus déroulants // Initialise the dropdow menus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Init_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'qCMCiprelProjectDataSet.Category' table. You can move, or remove it, as needed.
            //this.categoryTableAdapter.Fill(this.qCMCiprelProjectDataSet.Category);

            Dictionary<int, string> quizzSize = new Dictionary<int, string>();
            quizzSize.Add(1, "Petit (10 questions)");
            quizzSize.Add(2, "Moyen (20 questions)");
            quizzSize.Add(3, "Grand (40 questions)");

            quizz_size.DataSource = new BindingSource(quizzSize, null);
            quizz_size.DisplayMember = "Value";
            quizz_size.ValueMember = "Key";

            Dictionary<int, string> quizzLevel = new Dictionary<int, string>();
            quizzLevel.Add(1, "Novice");
            quizzLevel.Add(2, "Intermédiaire");
            quizzLevel.Add(3, "Avancé");

            level_selector.DataSource = new BindingSource(quizzLevel, null);
            level_selector.DisplayMember = "Value";
            level_selector.ValueMember = "Key";
            
            Dictionary<long, string> categories = new Dictionary<long, string>();

            try
            {
                SqlConnection conn = new SqlConnection(Properties.Resources.DatabaseConnectionString2);

                conn.Open();
                List<Category> categoryList = new List<Category>();
                SqlCommand cmd = new SqlCommand(@"SELECT * FROM Category", conn);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                    categoryList.Add(new Category(reader.GetInt64(0), reader.GetString(1)));
                reader.Close();

                foreach (Category cat in categoryList) categories.Add(cat.id, cat.name);

                if (categories.Count > 0)
                {
                    category_selector.DataSource = new BindingSource(categories, null);
                    category_selector.DisplayMember = "Value";
                    category_selector.ValueMember = "Key";
                }

                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter au server", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.Write(ex);
                Close();
            }
        }

        /// <summary>
        ///     Lance le QCM // Starts the quizz
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void start_Click(object sender, EventArgs e)
        {
            name_error.Visible = false;
            email_error.Visible = false;

            if (firstNameBox.Text.Length == 0)
            {
                name_error.Visible = true;
                if (lastNameBox.Text.Length != 0)
                    return;
            }

            if (lastNameBox.Text.Length == 0)
            {
                email_error.Visible = true;
                return;
            }

            new QCMForm(((KeyValuePair<long, string>)category_selector.SelectedItem).Key, ((KeyValuePair<long, string>)category_selector.SelectedItem).Value,
                ((KeyValuePair<int, string>)level_selector.SelectedItem).Key, ((KeyValuePair<int, string>)quizz_size.SelectedItem).Key,
                new LoggedUser(firstNameBox.Text, lastNameBox.Text, reg_nbr_box.Text)).Show();

            //Hide();
        }

        /// <summary>
        ///     Ferme le programme // Quit the program
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void quit_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        ///     Affiche le logo avec un fondu // Displays the logo with a fadein effect
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (opacity < 100)
            {
                float opacityvalue = (float)(opacity) / 100;
                logoCiprel.Image = ChangeOpacity(logoHolder.Image, opacityvalue);
                opacity++;
            }
            else
            {
                timer1.Stop();
            }
        }

        /// <summary>
        ///     Calcule le fondu // Calculates the fadein
        /// </summary>
        /// <param name="img"></param>
        /// <param name="opacityvalue"></param>
        /// <returns></returns>
        private Bitmap ChangeOpacity(Image img, float opacityvalue)
        {
            Bitmap bmp = new Bitmap(img.Width, img.Height);
            Graphics graphics = Graphics.FromImage(bmp);
            ColorMatrix colormatrix = new ColorMatrix();
            colormatrix.Matrix33 = opacityvalue;
            ImageAttributes imgAttribute = new ImageAttributes();
            imgAttribute.SetColorMatrix(colormatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
            graphics.DrawImage(img, new Rectangle(0, 0, bmp.Width, bmp.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imgAttribute);
            graphics.Dispose();
            return bmp;
        }

        /// <summary>
        ///     Ouvre l'interface d'administraion // Open the management interface
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void adminButton_Click(object sender, EventArgs e)
        {
            new ManagerForm(this).Show();
            Hide();
        }
    }
}
