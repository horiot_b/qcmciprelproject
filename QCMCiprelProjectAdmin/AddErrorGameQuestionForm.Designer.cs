﻿using System.Windows.Forms;

namespace QCMCiprelProjectAdmin
{
    partial class AddErrorGameQuestionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddErrorGameQuestionForm));
            this.questionContent = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.categoriesComboBox = new System.Windows.Forms.ComboBox();
            this.categoryBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.qCMCiprelProjectDataSet = new QCMCiprelProjectAdmin.QCMCiprelProjectDataSet();
            this.categoryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.categoryTableAdapter = new QCMCiprelProjectAdmin.QCMCiprelProjectDataSetTableAdapters.CategoryTableAdapter();
            this.label2 = new System.Windows.Forms.Label();
            this.addEditPictureButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.levelComboBox = new System.Windows.Forms.ComboBox();
            this.categoryBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.helpButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.questionComment = new System.Windows.Forms.TextBox();
            this.errorMessage = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.categoryBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qCMCiprelProjectDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoryBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // questionContent
            // 
            this.questionContent.Location = new System.Drawing.Point(12, 119);
            this.questionContent.Name = "questionContent";
            this.questionContent.Size = new System.Drawing.Size(1133, 20);
            this.questionContent.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Catégorie";
            // 
            // categoriesComboBox
            // 
            this.categoriesComboBox.DataSource = this.categoryBindingSource2;
            this.categoriesComboBox.DisplayMember = "name";
            this.categoriesComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.categoriesComboBox.FormattingEnabled = true;
            this.categoriesComboBox.Location = new System.Drawing.Point(12, 25);
            this.categoriesComboBox.Name = "categoriesComboBox";
            this.categoriesComboBox.Size = new System.Drawing.Size(493, 21);
            this.categoriesComboBox.TabIndex = 1;
            // 
            // categoryBindingSource2
            // 
            this.categoryBindingSource2.DataMember = "Category";
            this.categoryBindingSource2.DataSource = this.qCMCiprelProjectDataSet;
            // 
            // qCMCiprelProjectDataSet
            // 
            this.qCMCiprelProjectDataSet.DataSetName = "QCMCiprelProjectDataSet";
            this.qCMCiprelProjectDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // categoryBindingSource
            // 
            this.categoryBindingSource.DataMember = "Category";
            this.categoryBindingSource.DataSource = this.qCMCiprelProjectDataSet;
            // 
            // categoryTableAdapter
            // 
            this.categoryTableAdapter.ClearBeforeFill = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Question";
            // 
            // addEditPictureButton
            // 
            this.addEditPictureButton.Location = new System.Drawing.Point(512, 9);
            this.addEditPictureButton.Name = "addEditPictureButton";
            this.addEditPictureButton.Size = new System.Drawing.Size(207, 82);
            this.addEditPictureButton.TabIndex = 5;
            this.addEditPictureButton.Text = "Ajouter / Modifier l\'image";
            this.addEditPictureButton.UseVisualStyleBackColor = true;
            this.addEditPictureButton.Click += new System.EventHandler(this.addEditPictureButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(725, 9);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(207, 82);
            this.saveButton.TabIndex = 6;
            this.saveButton.Text = "Enregistrer";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(937, 9);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(207, 39);
            this.cancelButton.TabIndex = 7;
            this.cancelButton.Text = "Annuler";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(13, 189);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1132, 599);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Niveau";
            // 
            // levelComboBox
            // 
            this.levelComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.levelComboBox.FormattingEnabled = true;
            this.levelComboBox.Location = new System.Drawing.Point(12, 70);
            this.levelComboBox.Name = "levelComboBox";
            this.levelComboBox.Size = new System.Drawing.Size(493, 21);
            this.levelComboBox.TabIndex = 2;
            // 
            // categoryBindingSource1
            // 
            this.categoryBindingSource1.DataMember = "Category";
            this.categoryBindingSource1.DataSource = this.qCMCiprelProjectDataSet;
            // 
            // helpButton
            // 
            this.helpButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.helpButton.Location = new System.Drawing.Point(938, 54);
            this.helpButton.Name = "helpButton";
            this.helpButton.Size = new System.Drawing.Size(207, 37);
            this.helpButton.TabIndex = 8;
            this.helpButton.Text = "Aide";
            this.helpButton.UseVisualStyleBackColor = true;
            this.helpButton.Click += new System.EventHandler(this.helpButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 146);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Commentaire";
            // 
            // questionComment
            // 
            this.questionComment.Location = new System.Drawing.Point(12, 163);
            this.questionComment.Name = "questionComment";
            this.questionComment.Size = new System.Drawing.Size(1133, 20);
            this.questionComment.TabIndex = 4;
            // 
            // errorMessage
            // 
            this.errorMessage.ForeColor = System.Drawing.Color.DarkRed;
            this.errorMessage.Location = new System.Drawing.Point(86, 146);
            this.errorMessage.Name = "errorMessage";
            this.errorMessage.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.errorMessage.Size = new System.Drawing.Size(1058, 17);
            this.errorMessage.TabIndex = 14;
            this.errorMessage.Text = "ERROR";
            this.errorMessage.Visible = false;
            // 
            // AddErrorGameQuestionForm
            // 
            this.AcceptButton = this.saveButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(1156, 800);
            this.Controls.Add(this.errorMessage);
            this.Controls.Add(this.questionComment);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.helpButton);
            this.Controls.Add(this.levelComboBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.addEditPictureButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.categoriesComboBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.questionContent);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AddErrorGameQuestionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ajouter une question \'Jeu des erreurs\'";
            this.Load += new System.EventHandler(this.AddErrorGameQuestionForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.categoryBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qCMCiprelProjectDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoryBindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox questionContent;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox categoriesComboBox;
        private QCMCiprelProjectDataSet qCMCiprelProjectDataSet;
        private System.Windows.Forms.BindingSource categoryBindingSource;
        private QCMCiprelProjectDataSetTableAdapters.CategoryTableAdapter categoryTableAdapter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button addEditPictureButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox levelComboBox;
        private System.Windows.Forms.BindingSource categoryBindingSource1;
        private Button helpButton;
        private Label label4;
        private TextBox questionComment;
        private Label errorMessage;
        private BindingSource categoryBindingSource2;
    }
}