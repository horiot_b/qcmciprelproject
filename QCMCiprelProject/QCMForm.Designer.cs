﻿namespace QCMCiprelProject
{
    partial class QCMForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QCMForm));
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.questionImage = new System.Windows.Forms.PictureBox();
            this.next = new System.Windows.Forms.Button();
            this.skip = new System.Windows.Forms.Button();
            this.question = new System.Windows.Forms.Label();
            this.comment = new System.Windows.Forms.Label();
            this.quit = new System.Windows.Forms.Button();
            this.errorGameImage = new System.Windows.Forms.PictureBox();
            this.questionErrorImage = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.questionImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorGameImage)).BeginInit();
            this.SuspendLayout();
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(-4, 571);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(1052, 10);
            this.progressBar.TabIndex = 1;
            // 
            // radioButton1
            // 
            this.radioButton1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radioButton1.Location = new System.Drawing.Point(12, 205);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(314, 53);
            this.radioButton1.TabIndex = 96;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "radioButton1";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.Location = new System.Drawing.Point(12, 264);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(314, 53);
            this.radioButton2.TabIndex = 97;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "radioButton2";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.Location = new System.Drawing.Point(12, 324);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(314, 53);
            this.radioButton3.TabIndex = 98;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "radioButton3";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton4
            // 
            this.radioButton4.Location = new System.Drawing.Point(12, 381);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(314, 53);
            this.radioButton4.TabIndex = 99;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "radioButton4";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // questionImage
            // 
            this.questionImage.Dock = System.Windows.Forms.DockStyle.Right;
            this.questionImage.Location = new System.Drawing.Point(332, 0);
            this.questionImage.MaximumSize = new System.Drawing.Size(709, 418);
            this.questionImage.Name = "questionImage";
            this.questionImage.Size = new System.Drawing.Size(709, 418);
            this.questionImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.questionImage.TabIndex = 6;
            this.questionImage.TabStop = false;
            // 
            // next
            // 
            this.next.Location = new System.Drawing.Point(829, 461);
            this.next.Name = "next";
            this.next.Size = new System.Drawing.Size(200, 75);
            this.next.TabIndex = 7;
            this.next.Text = "Valider";
            this.next.UseVisualStyleBackColor = true;
            this.next.Click += new System.EventHandler(this.next_Click);
            // 
            // skip
            // 
            this.skip.Location = new System.Drawing.Point(623, 461);
            this.skip.Name = "skip";
            this.skip.Size = new System.Drawing.Size(200, 75);
            this.skip.TabIndex = 8;
            this.skip.Text = "Répondre plus tard";
            this.skip.UseVisualStyleBackColor = true;
            this.skip.Click += new System.EventHandler(this.skip_Click);
            // 
            // question
            // 
            this.question.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.question.Location = new System.Drawing.Point(9, 9);
            this.question.Name = "question";
            this.question.Size = new System.Drawing.Size(317, 193);
            this.question.TabIndex = 9;
            this.question.Text = "question";
            // 
            // comment
            // 
            this.comment.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comment.Location = new System.Drawing.Point(9, 458);
            this.comment.Name = "comment";
            this.comment.Size = new System.Drawing.Size(608, 107);
            this.comment.TabIndex = 10;
            this.comment.Text = "comment";
            // 
            // quit
            // 
            this.quit.Location = new System.Drawing.Point(623, 542);
            this.quit.Name = "quit";
            this.quit.Size = new System.Drawing.Size(406, 23);
            this.quit.TabIndex = 11;
            this.quit.Text = "Menu principal";
            this.quit.UseVisualStyleBackColor = true;
            this.quit.Click += new System.EventHandler(this.quit_Click);
            // 
            // errorGameImage
            // 
            this.errorGameImage.Location = new System.Drawing.Point(12, 59);
            this.errorGameImage.Name = "errorGameImage";
            this.errorGameImage.Size = new System.Drawing.Size(1017, 396);
            this.errorGameImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.errorGameImage.TabIndex = 100;
            this.errorGameImage.TabStop = false;
            this.errorGameImage.Click += new System.EventHandler(this.errorGameImage_Click);
            // 
            // questionErrorImage
            // 
            this.questionErrorImage.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.questionErrorImage.Location = new System.Drawing.Point(12, 9);
            this.questionErrorImage.Name = "questionErrorImage";
            this.questionErrorImage.Size = new System.Drawing.Size(1017, 47);
            this.questionErrorImage.TabIndex = 101;
            this.questionErrorImage.Text = "label1";
            // 
            // QCMForm
            // 
            this.AcceptButton = this.next;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1041, 579);
            this.Controls.Add(this.quit);
            this.Controls.Add(this.comment);
            this.Controls.Add(this.question);
            this.Controls.Add(this.skip);
            this.Controls.Add(this.next);
            this.Controls.Add(this.questionImage);
            this.Controls.Add(this.radioButton4);
            this.Controls.Add(this.radioButton3);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.questionErrorImage);
            this.Controls.Add(this.errorGameImage);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "QCMForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Question n°x";
            this.Load += new System.EventHandler(this.Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.questionImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorGameImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.PictureBox questionImage;
        private System.Windows.Forms.Button next;
        private System.Windows.Forms.Button skip;
        private System.Windows.Forms.Label question;
        private System.Windows.Forms.Label comment;
        private System.Windows.Forms.Button quit;
        private System.Windows.Forms.PictureBox errorGameImage;
        private System.Windows.Forms.Label questionErrorImage;
    }
}

