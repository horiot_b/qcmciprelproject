﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;

namespace QCMCiprelProject
{
    /// <summary>
    ///     Fenêtre de questionnaire // Quizz window
    /// </summary>
    public partial class QCMForm : Form
    {
        private List<KeyValuePair<int, int>> answers;
        private List<Question> questions;
        private int questionIndex;
        private Quizz quizz;
        private Question quizzQuestion;
        private LoggedUser user;
        private bool showComment;
        private string categoryName;
        private int level;
        private List<Point> answersErrorGame = new List<Point>();
        private List<Rectangle> coordsErrorGame = new List<Rectangle>();
        private Graphics graphics;

        /// <summary>
        ///     Constructeur avec paramètre // Constructor with parameter
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="categoryName"></param>
        /// <param name="level"></param>
        /// <param name="size"></param>
        /// <param name="user"></param>
        public QCMForm(long categoryId, string categoryName, int level, int size, LoggedUser user)
        {
            InitializeComponent();

            quizz = new Quizz(categoryId, level, size);

            if (quizz.getSize() > 0)
            {

                answers = new List<KeyValuePair<int, int>>();
                questions = new List<Question>();

                progressBar.Minimum = 0;
                progressBar.Maximum = 100;
                progressBar.Value = 100 / quizz.getSize();

                this.user = user;

                showComment = false;
                questionIndex = 1;
                this.categoryName = categoryName;
                this.level = level;
                graphics = errorGameImage.CreateGraphics();
            }

        }

        /// <summary>
        ///     Gère les évènements lorsque l'utilisateur appuie sur 'suivant' // Manages the events when the user is clicking on 'next'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void next_Click(object sender, EventArgs e)
        {
            int index = 0;
            bool beenChecked = false;
            bool answered = false;

            #region Vérification réponse
            if (!quizzQuestion.isErrorGame)
            {
                // Vérification qu'une réponse ai été sélectionnée et récupération de cette réponse
                // Checking that an aswer as been selected and getting this answer
                foreach (RadioButton rb in this.Controls.OfType<RadioButton>())
                {
                    if (rb.Checked)
                    {
                        beenChecked = true;
                        if (showComment)
                        {
                            if (quizzQuestion.answers.ElementAt(index).correctAnswer)
                                answers.Add(new KeyValuePair<int, int>(index, 100));
                            else
                                answers.Add(new KeyValuePair<int, int>(index, 0));
                        }
                    }

                    if (!beenChecked) index++;
                }

                if (!index.Equals(quizzQuestion.answers.Count))
                {
                    // Affichage des commentaires et coloration des réponses
                    // Displaying comments and answer coloration
                    if (!showComment)
                    {
                        if (quizzQuestion.answers.ElementAt(index).correctAnswer) comment.Text = quizzQuestion.commentCorrectAnswer;
                        else comment.Text = quizzQuestion.commentIncorrectAnswer;

                        showComment = true;

                        int tmp = 0;
                        foreach (RadioButton rb in this.Controls.OfType<RadioButton>())
                        {
                            if (tmp < quizzQuestion.answers.Count)
                                if (quizzQuestion.answers.ElementAt(tmp).correctAnswer) rb.ForeColor = Color.FromArgb(0, 155, 0);
                                else rb.ForeColor = Color.FromArgb(155, 0, 0);
                            rb.AutoCheck = false;
                            skip.Visible = false;
                            tmp++;
                        }

                    }
                    else
                        answered = true;
                }
            }
            #endregion
            else
            {
                int correctAnswers = 0;
                foreach (Answer answer in quizzQuestion.answers)
                {
                    string[] strs = answer.content.Split(';');
                    Point coords = new Point(int.Parse(strs[0]), int.Parse(strs[1]));
                    Size size = new Size(int.Parse(strs[2]), int.Parse(strs[3]));
                    Rectangle rect = new Rectangle(coords, size);
                    if (showComment)
                        foreach (Point tmp in answersErrorGame)
                        {
                            if (rect.Contains(tmp))
                            {
                                correctAnswers++;
                                break;
                            }
                        }
                    else
                    {
                        skip.Visible = false;
                        graphics.DrawRectangle(new Pen(Brushes.LawnGreen, 3), reverseRect(rect));
                    }
                }
                if (showComment)
                {
                    answers.Add(new KeyValuePair<int, int>(index, correctAnswers * 100 / quizzQuestion.answers.Count));
                    answered = true;
                }
                else
                    showComment = true;
            }
            //TODO replacer
            #region Passage question suivante
            // Passage à la question suivante
            // Go to next question
            if (answered)
            {
                questionIndex++;
                quizz.changeQuestionState(quizzQuestion.id, State.Answered);

                if (questionIndex <= quizz.getSize()) progressBar.Value += 100 / quizz.getSize();

                showComment = false;
                Form_Load(sender, e);
            }
            #endregion
        }

        /// <summary>
        ///     Gère les évènements lorsque l'utilisateur choisit de répondre plus tard à une question
        ///     // Manage the events when the user clicks on 'answer later'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void skip_Click(object sender, EventArgs e)
        {
            if (quizzQuestion.state.Equals(State.Skipped))
            {
                quizz.changeQuestionState(quizzQuestion.id, State.Unanswered);
                answers.Add(new KeyValuePair<int, int>(99, 0));
                questionIndex++;
                if (questionIndex <= quizz.getSize()) progressBar.Value += 100 / quizz.getSize();

            }
            else quizz.changeQuestionState(quizzQuestion.id, State.Skipped);

            Form_Load(sender, e);
        }

        /// <summary>
        ///     Initialise la question, l'images, le commentaire et les réponses
        ///     // Initialise the question, the picture, the comment and the answer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_Load(object sender, EventArgs e)
        {

            if (quizz.getSize() == 0)
            {
                MessageBox.Show("Il n'y a pas de questions répondant aux critères sélectionnés");
                //initForm.Show();
                Close();
                return;
            }

            #region Fin du quizz
            if (questionIndex > quizz.getSize())
            {
                int score = 0;

                foreach (KeyValuePair<int, int> pair in answers) score += pair.Value;

                new ReportForm(score / answers.Count, categoryName, level, user, questions, answers).Show();
                //returnMainMenu = false;
                Close();
                return;
            }
            #endregion

            #region Init question

            /*MouseEventArgs me = (MouseEventArgs)e;
            Point coordinates = me.Location;*/

            quizzQuestion = quizz.getRandomQuestionByState(State.New);

            if (quizzQuestion == null)
            {
                quizzQuestion = quizz.getRandomQuestionByState(State.Skipped);
                skip.Text = "Ne pas répondre";
            }


            if (!quizzQuestion.isErrorGame)
            {
                //TODO Change visibility answers / picturebox
                int index = 0;
                quizzQuestion.shuffleAnswers();
                foreach (RadioButton rb in this.Controls.OfType<RadioButton>())
                {
                    rb.Checked = false;
                    rb.AutoCheck = true;
                    rb.ForeColor = Color.FromArgb(0, 0, 0);
                    if (index < quizzQuestion.answers.Count)
                    {
                        rb.Text = quizzQuestion.answers.ElementAt(index++).content;
                        rb.Visible = true;
                    }
                    else rb.Visible = false;
                }
                question.Text = quizzQuestion.question;
                questionImage.Image = quizzQuestion.image;

                question.Visible = true;
                questionImage.Visible = true;
                errorGameImage.Visible = false;
                questionErrorImage.Visible = false;
            }
            else
            {
                errorGameImage.Visible = true;
                questionErrorImage.Visible = true;
                questionImage.Visible = false;
                question.Visible = false;
                foreach (RadioButton rb in this.Controls.OfType<RadioButton>())
                    rb.Visible = false;

                errorGameImage.Image = quizzQuestion.image;
                questionErrorImage.Text = quizzQuestion.question;
                answersErrorGame.Clear();
                coordsErrorGame.Clear();
            }

            skip.Visible = true;
            comment.Text = quizzQuestion.commentQuestion;
            Text = "Question n°" + (questionIndex + 1);
            if (quizzQuestion.state.Equals(State.New))
                questions.Add(quizzQuestion);
            #endregion
        }

        /// <summary>
        ///     Retourne au menu principal
        ///     // Return to main menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void quit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void errorGameImage_Click(object sender, EventArgs e)
        {
            MouseEventArgs me = (MouseEventArgs)e;
            Point coordinates = convertCoordinates(me.Location);

            if (coordinates.Equals(me.Location)) return;

            Rectangle currentRect = new Rectangle(me.Location.X - 8, me.Location.Y - 8, 16, 16);
            bool deleted = false;

            foreach (Rectangle rect in coordsErrorGame)
            {
                if (rect.Contains(me.Location))
                {
                    deleted = true;
                    answersErrorGame.RemoveAt(coordsErrorGame.IndexOf(rect));
                    coordsErrorGame.Remove(rect);
                    errorGameImage.Image = quizzQuestion.image;
                    Invalidate();
                    Update();
                    foreach (Rectangle tmp in coordsErrorGame)
                        graphics.DrawRectangle(new Pen(Brushes.SkyBlue, 3), tmp);
                    break;
                }
            }

            if (!deleted)
            {
                coordsErrorGame.Add(currentRect);
                answersErrorGame.Add(coordinates);
                graphics.DrawRectangle(new Pen(Brushes.SkyBlue, 3), currentRect);
            }
        }

        private Point convertCoordinates(Point coordinates)
        {
            // test to make sure our image is not null
            if (errorGameImage.Image == null) return coordinates;
            // Make sure our control width and height are not 0 and our 
            // image width and height are not 0
            if (errorGameImage.Width <= 0 || errorGameImage.Height <= 0 ||
                errorGameImage.Image.Width <= 0 || errorGameImage.Image.Height <= 0) return coordinates;
            // This is the one that gets a little tricky. Essentially, need to check 
            // the aspect ratio of the image to the aspect ratio of the control
            // to determine how it is being rendered
            float imageAspect = (float)errorGameImage.Image.Width / errorGameImage.Image.Height;
            float controlAspect = (float)errorGameImage.Width / errorGameImage.Height;
            float newX = coordinates.X;
            float newY = coordinates.Y;
            if (imageAspect > controlAspect)
            {
                // This means that we are limited by width, 
                // meaning the image fills up the entire control from left to right
                float ratioWidth = (float)errorGameImage.Image.Width / errorGameImage.Width;
                newX *= ratioWidth;
                float scale = (float)errorGameImage.Width / errorGameImage.Image.Width;
                float displayHeight = scale * errorGameImage.Image.Height;
                float diffHeight = errorGameImage.Height - displayHeight;
                diffHeight /= 2;
                newY -= diffHeight;
                newY /= scale;
            }
            else
            {
                // This means that we are limited by height, 
                // meaning the image fills up the entire control from top to bottom
                float ratioHeight = (float)errorGameImage.Image.Height / errorGameImage.Height;
                newY *= ratioHeight;
                float scale = (float)errorGameImage.Height / errorGameImage.Image.Height;
                float displayWidth = scale * errorGameImage.Image.Width;
                float diffWidth = errorGameImage.Width - displayWidth;
                diffWidth /= 2;
                newX -= diffWidth;
                newX /= scale;
            }
            if (newX < 0 || newY < 0 || newX > errorGameImage.Image.Width || newY > errorGameImage.Image.Height) return coordinates;
            return new Point((int)newX, (int)newY);
        }

        private Rectangle reverseRect(Rectangle rect)
        {
            if (errorGameImage.Image == null) return rect;
            if (errorGameImage.Width <= 0 || errorGameImage.Height <= 0 ||
                errorGameImage.Image.Width <= 0 || errorGameImage.Image.Height <= 0) return rect;
            float imageAspect = (float)errorGameImage.Image.Width / errorGameImage.Image.Height;
            float controlAspect = (float)errorGameImage.Width / errorGameImage.Height;
            if (imageAspect > controlAspect)
            {
                // This means that we are limited by width, 
                // meaning the image fills up the entire control from left to right
                float scale = (float)errorGameImage.Width / errorGameImage.Image.Width;
                float displayHeight = scale * errorGameImage.Image.Height;
                float diffHeight = errorGameImage.Height - displayHeight;
                diffHeight /= 2;
                return new Rectangle((int)(rect.X * scale), (int)(rect.Y * scale + diffHeight),
                    (int)(rect.Width * scale), (int)(rect.Height * scale));
            }
            else
            {
                // This means that we are limited by height, 
                // meaning the image fills up the entire control from top to bottom
                float scale = (float)errorGameImage.Height / errorGameImage.Image.Height;
                float displayWidth = scale * errorGameImage.Image.Width;
                float diffWidth = errorGameImage.Width - displayWidth;
                diffWidth /= 2;
                return new Rectangle((int)(rect.X * scale + diffWidth), (int)(rect.Y * scale),
                    (int)(rect.Width * scale), (int)(rect.Height * scale));
            }
        }
    }
}
