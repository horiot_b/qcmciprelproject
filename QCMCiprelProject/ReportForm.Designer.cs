﻿namespace QCMCiprelProject
{
    partial class ReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportForm));
            this.successText = new System.Windows.Forms.Label();
            this.successRate = new System.Windows.Forms.Label();
            this.quit = new System.Windows.Forms.Button();
            this.userField = new System.Windows.Forms.Label();
            this.saveReport = new System.Windows.Forms.Button();
            this.logoPale = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.logoPale)).BeginInit();
            this.SuspendLayout();
            // 
            // successText
            // 
            this.successText.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.successText.Location = new System.Drawing.Point(12, 32);
            this.successText.Name = "successText";
            this.successText.Size = new System.Drawing.Size(575, 103);
            this.successText.TabIndex = 0;
            this.successText.Text = "Vous avez terminé un QCM sur\r\n%\r\nUn rapport de vos résultat peut être enregistré " +
    "en cliquant sur le bouton \"Enregistrer un rapport\"";
            this.successText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // successRate
            // 
            this.successRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.successRate.Location = new System.Drawing.Point(27, 135);
            this.successRate.Name = "successRate";
            this.successRate.Size = new System.Drawing.Size(560, 39);
            this.successRate.TabIndex = 1;
            this.successRate.Text = "Taux de bonne réponse : XX%";
            this.successRate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // quit
            // 
            this.quit.Location = new System.Drawing.Point(132, 234);
            this.quit.Name = "quit";
            this.quit.Size = new System.Drawing.Size(350, 40);
            this.quit.TabIndex = 2;
            this.quit.Text = "Menu principal";
            this.quit.UseVisualStyleBackColor = true;
            this.quit.Click += new System.EventHandler(this.quit_Click);
            // 
            // userField
            // 
            this.userField.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.userField.Location = new System.Drawing.Point(132, 9);
            this.userField.Name = "userField";
            this.userField.Size = new System.Drawing.Size(350, 23);
            this.userField.TabIndex = 3;
            this.userField.Text = "label1";
            this.userField.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // saveReport
            // 
            this.saveReport.Location = new System.Drawing.Point(132, 188);
            this.saveReport.Name = "saveReport";
            this.saveReport.Size = new System.Drawing.Size(350, 40);
            this.saveReport.TabIndex = 1;
            this.saveReport.Text = "Enregistrer un rapport";
            this.saveReport.UseVisualStyleBackColor = true;
            this.saveReport.Click += new System.EventHandler(this.saveReport_Click);
            // 
            // logoPale
            // 
            this.logoPale.Image = ((System.Drawing.Image)(resources.GetObject("logoPale.Image")));
            this.logoPale.InitialImage = null;
            this.logoPale.Location = new System.Drawing.Point(-1, -1);
            this.logoPale.Name = "logoPale";
            this.logoPale.Size = new System.Drawing.Size(24, 30);
            this.logoPale.TabIndex = 4;
            this.logoPale.TabStop = false;
            this.logoPale.Visible = false;
            // 
            // ReportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(599, 286);
            this.Controls.Add(this.logoPale);
            this.Controls.Add(this.saveReport);
            this.Controls.Add(this.userField);
            this.Controls.Add(this.quit);
            this.Controls.Add(this.successRate);
            this.Controls.Add(this.successText);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ReportForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Rapport de fin de questionnaire";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ReportForm_Closing);
            ((System.ComponentModel.ISupportInitialize)(this.logoPale)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label successText;
        private System.Windows.Forms.Label successRate;
        private System.Windows.Forms.Button quit;
        private System.Windows.Forms.Label userField;
        protected System.Windows.Forms.Button saveReport;
        private System.Windows.Forms.PictureBox logoPale;
    }
}