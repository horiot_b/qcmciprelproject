﻿using System;

using System.Windows.Forms;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System.Collections.Generic;

namespace QCMCiprelProject
{
    /// <summary>
    ///     Fenêtre de fin de questionnaire générant un rapport // Window displayed at the end of the quizz and generating a report
    /// </summary>
    public partial class ReportForm : Form
    {
        /// <summary>
        ///     Informations sur l'utilisateur ayant fait le questionnaire // User's data
        /// </summary>
        private LoggedUser user;

        /// <summary>
        ///     Liste des questions du questionnaires // Quizz's question's list
        /// </summary>
        private List<Question> questions;

        /// <summary>
        ///     Liste des réponses de l'utilisateur (index (1-4), bonne réponse?)
        ///     // User's answers's list (index (1-4), good answer?)
        /// </summary>
        private List<KeyValuePair<int, int>> answers;

        /// <summary>
        ///     Nom de la catégorie dans laquelle se trouvait le questionnaire
        ///     // Quizz's category's name
        /// </summary>
        private string categoryName;

        /// <summary>
        ///     Niveau de difficulté du quizz // Quizz's level
        /// </summary>
        private int level;

        /// <summary>
        ///     Constructeur avec paramètres // Constructor with parameters
        /// </summary>
        /// <param name="successRate"></param>
        /// <param name="categoryName"></param>
        /// <param name="initForm"></param>
        /// <param name="user"></param>
        /// <param name="questions"></param>
        /// <param name="answers"></param>
        public ReportForm(int successRate, string categoryName, int level, LoggedUser user, List<Question> questions,
            List<KeyValuePair<int, int>> answers)
        {
            InitializeComponent();
            this.successRate.Text = "Taux de réussite : " + successRate.ToString() + '%';
            successText.Text = successText.Text.Replace("%", categoryName);

            this.userField.Text = user.firstName + ' ' + user.lastName;
            //this.initForm = initForm;
            this.categoryName = categoryName;
            this.level = level;

            this.user = user;
            this.questions = questions;
            this.answers = answers;
        }

        /// <summary>
        ///     Ferme cette fenêtre et affiche le menu princpal // Close this window and display the main menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void quit_Click(object sender, EventArgs e)
        {
            //initForm.Show();
            Close();
        }

        /// <summary>
        ///     Génère un rapport au format PDF et l'enregistre sur le disque // Generates a PDF report and saves it on disk
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveReport_Click(object sender, EventArgs e)
        {
            #region Génération rapport
            // Création du document // Report's creation
            PdfDocument document = new PdfDocument();

            document.Info.Title = "Rapport QCMCiprel-" + user.firstName + ' ' + user.lastName + '-' + categoryName +
                    '-' + System.DateTime.Now.ToString().Replace('/', '_').Replace(':', '_'); //TODO email

            // Ajout d'une page vide au document // Add an empty page to the document
            PdfPage page = document.AddPage();

            // Récupération d'un pointeur sur la page permettant d'écrire dessus
            // Getting a pointer on the page to draw on it
            XGraphics gfx = XGraphics.FromPdfPage(page);

            // Génération des polices d'écriture // Font's generation
            XFont verdanaBold = new XFont("Verdana", 18, XFontStyle.Bold);
            XFont verdana = new XFont("Verdana", 12, XFontStyle.Regular);
            XFont verdanaSmall = new XFont("Verdana", 10, XFontStyle.Regular);

            // Affichage du logo sur la page // Displays the logo on the page
            gfx.DrawImage(logoPale.Image, 50, 50);
            gfx.DrawLine(new XPen(XBrushes.Gray.Color, 1), 151, 48, 151, 72);
            gfx.DrawString("COMPAGNIE IVOIRIENNE", verdana, XBrushes.Gray, 161, 58);
            gfx.DrawString("DE PRODUCTION D'ELECTRICITE", verdana, XBrushes.Gray, 161, 70);

            // Affichage du titre du document // Display the document's title
            gfx.DrawString("Rapport QCM Ciprel", verdanaBold, XBrushes.Black, new XRect(0, 90, page.Width, 110), XStringFormats.Center);

            // Affichage des informations sur l'utilisateur // Displays user's data
            gfx.DrawString("Prénom : " + user.firstName, verdana, XBrushes.Black, 50, 275);
            gfx.DrawString("Nom : " + user.lastName, verdana, XBrushes.Black, 50, 300);
            if (user.regNbr.Length > 0)
            {
                gfx.DrawString("Matricule : " + user.regNbr, verdana, XBrushes.Black, 50, 325);
                gfx.DrawString("Série : " + categoryName, verdana, XBrushes.Black, 50, 350);
                gfx.DrawString("Difficulté : " + level, verdana, XBrushes.Black, 50, 375);
            }
            else
            {
                gfx.DrawString("Série : " + categoryName, verdana, XBrushes.Black, 50, 325);
                gfx.DrawString("Difficulté : " + level, verdana, XBrushes.Black, 50, 350);
            }
            gfx.DrawString(successRate.Text, verdanaBold, XBrushes.Black,
              new XRect(0, 50, page.Width, page.Height),
              XStringFormats.Center);

            // Affichage des questions dans le rapport // Displays the questions on the report
            int questionIndex = 0;
            int reset = 0;
            XGraphics questionGfx = null;
            while (questionIndex < questions.Count)
            {
                // Seulement 17 questions peuvent tenir sur une page
                // La génération d'une page supplémentaire avec un logo est donc nécessaire
                // Only 17 questions can fit on a page, generating an additional page is necessary
                if (reset % 17 == 0)
                {
                    PdfPage questionPage = document.AddPage();
                    questionGfx = XGraphics.FromPdfPage(questionPage);

                    questionGfx.DrawImage(logoPale.Image, 50, 50);
                    questionGfx.DrawLine(new XPen(XBrushes.Gray.Color, 1), 151, 48, 151, 72);
                    questionGfx.DrawString("COMPAGNIE IVOIRIENNE", verdana, XBrushes.Gray, 161, 58);
                    questionGfx.DrawString("DE PRODUCTION D'ELECTRICITE", verdana, XBrushes.Gray, 161, 70);
                }

                // Affichage de la question // Displays the question
                string questionString = questions[questionIndex].question;
                if (questionString.Length > 70)
                {
                    questionString = questionString.Insert(70, "\\/");
                    questionString = questionString.Split("\\/".ToCharArray())[0];
                    questionString = questionString.Insert(questionString.Length, "...");
                }
                questionGfx.DrawString((questionIndex + 1) + "- " + questionString, verdana, XBrushes.Black, 50, 125 + (reset * 40));

                // Affiche la réponse de l'utilisateur et la colore en fonction de son exactitude
                // Displays the user's answer and color in according to its accuracy
                if (answers[questionIndex].Value == 100)
                {
                    if (!questions[questionIndex].isErrorGame)
                        questionGfx.DrawString(questions[questionIndex].answers[answers[questionIndex].Key].content,
                            verdanaSmall, XBrushes.DarkGreen, 75, 140 + (reset * 40));
                    else
                        questionGfx.DrawString("Eléments corrects : " + answers[questionIndex].Value + '%',
                            verdanaSmall, XBrushes.DarkGreen, 75, 140 + (reset * 40));
                }
                else
                {
                    if (answers[questionIndex].Key != 99 && !questions[questionIndex].isErrorGame)
                        questionGfx.DrawString(questions[questionIndex].answers[answers[questionIndex].Key].content,
                            verdanaSmall, XBrushes.DarkRed, 75, 140 + (reset * 40));
                    else if (questions[questionIndex].isErrorGame)
                    {
                        Console.Out.WriteLine("VALUE : " + answers[questionIndex].Value);
                        if (answers[questionIndex].Value > 50)
                            questionGfx.DrawString("Eléments corrects : " + answers[questionIndex].Value + '%',
                                verdanaSmall, XBrushes.DarkOrange, 75, 140 + (reset * 40));
                        else
                            questionGfx.DrawString("Eléments corrects : " + answers[questionIndex].Value + '%',
                                verdanaSmall, XBrushes.DarkRed, 75, 140 + (reset * 40));
                    }
                    else questionGfx.DrawString("PAS DE REPONSE", verdanaSmall, XBrushes.DarkRed, 75, 140 + (reset * 40));
                }
                questionIndex++;
                reset++;
            }
            #endregion

            // Sauvegarde du rapport // Save the report
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Pdf Files|*.pdf";
            saveFileDialog.Title = "Enregistrer le rapport";
            saveFileDialog.FileName = document.Info.Title;

            if (saveFileDialog.ShowDialog() == DialogResult.OK) document.Save(saveFileDialog.FileName);

            // Le bouton de génération est caché pour ne pas générer plusieurs fois le même rapport
            // The generation button is hidden to prevent the generaration of the same report twice
            saveReport.Visible = false;
        }

        private void ReportForm_Closing(object sender, EventArgs e)
        {
            //initForm.Show();
        }
    }
}
