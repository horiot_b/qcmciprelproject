﻿namespace QCMCiprelProject
{
    partial class InitForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InitForm));
            this.firstNameBox = new System.Windows.Forms.TextBox();
            this.name = new System.Windows.Forms.Label();
            this.email = new System.Windows.Forms.Label();
            this.lastNameBox = new System.Windows.Forms.TextBox();
            this.category = new System.Windows.Forms.Label();
            this.category_selector = new System.Windows.Forms.ComboBox();
            this.logoCiprel = new System.Windows.Forms.PictureBox();
            this.start = new System.Windows.Forms.Button();
            this.quit = new System.Windows.Forms.Button();
            this.quizzSize = new System.Windows.Forms.Label();
            this.quizz_size = new System.Windows.Forms.ComboBox();
            this.reg_nbr = new System.Windows.Forms.Label();
            this.reg_nbr_box = new System.Windows.Forms.TextBox();
            this.name_error = new System.Windows.Forms.Label();
            this.email_error = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.logoHolder = new System.Windows.Forms.PictureBox();
            this.mandatoryFields = new System.Windows.Forms.Label();
            this.level_selector = new System.Windows.Forms.ComboBox();
            this.level = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.logoCiprel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.logoHolder)).BeginInit();
            this.SuspendLayout();
            // 
            // firstNameBox
            // 
            this.firstNameBox.Location = new System.Drawing.Point(12, 29);
            this.firstNameBox.Name = "firstNameBox";
            this.firstNameBox.Size = new System.Drawing.Size(350, 20);
            this.firstNameBox.TabIndex = 0;
            // 
            // name
            // 
            this.name.AutoSize = true;
            this.name.Location = new System.Drawing.Point(9, 13);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(47, 13);
            this.name.TabIndex = 1;
            this.name.Text = "Prénom*";
            // 
            // email
            // 
            this.email.AutoSize = true;
            this.email.Location = new System.Drawing.Point(9, 73);
            this.email.Name = "email";
            this.email.Size = new System.Drawing.Size(36, 13);
            this.email.TabIndex = 2;
            this.email.Text = "NOM*";
            // 
            // lastNameBox
            // 
            this.lastNameBox.Location = new System.Drawing.Point(12, 89);
            this.lastNameBox.Name = "lastNameBox";
            this.lastNameBox.Size = new System.Drawing.Size(350, 20);
            this.lastNameBox.TabIndex = 1;
            // 
            // category
            // 
            this.category.AutoSize = true;
            this.category.Location = new System.Drawing.Point(9, 206);
            this.category.Name = "category";
            this.category.Size = new System.Drawing.Size(136, 13);
            this.category.TabIndex = 4;
            this.category.Text = "Sélectionnez une catégorie";
            // 
            // category_selector
            // 
            this.category_selector.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.category_selector.FormattingEnabled = true;
            this.category_selector.Location = new System.Drawing.Point(12, 222);
            this.category_selector.Name = "category_selector";
            this.category_selector.Size = new System.Drawing.Size(350, 21);
            this.category_selector.TabIndex = 3;
            // 
            // logoCiprel
            // 
            this.logoCiprel.Location = new System.Drawing.Point(383, 259);
            this.logoCiprel.Name = "logoCiprel";
            this.logoCiprel.Size = new System.Drawing.Size(337, 80);
            this.logoCiprel.TabIndex = 6;
            this.logoCiprel.TabStop = false;
            // 
            // start
            // 
            this.start.Location = new System.Drawing.Point(383, 29);
            this.start.Name = "start";
            this.start.Size = new System.Drawing.Size(337, 99);
            this.start.TabIndex = 5;
            this.start.Text = "Démarrer le quizz";
            this.start.UseVisualStyleBackColor = true;
            this.start.Click += new System.EventHandler(this.start_Click);
            // 
            // quit
            // 
            this.quit.Location = new System.Drawing.Point(383, 134);
            this.quit.Name = "quit";
            this.quit.Size = new System.Drawing.Size(337, 40);
            this.quit.TabIndex = 6;
            this.quit.Text = "Quitter";
            this.quit.UseVisualStyleBackColor = true;
            this.quit.Click += new System.EventHandler(this.quit_Click);
            // 
            // quizzSize
            // 
            this.quizzSize.AutoSize = true;
            this.quizzSize.Location = new System.Drawing.Point(9, 302);
            this.quizzSize.Name = "quizzSize";
            this.quizzSize.Size = new System.Drawing.Size(74, 13);
            this.quizzSize.TabIndex = 9;
            this.quizzSize.Text = "Taille du quizz";
            // 
            // quizz_size
            // 
            this.quizz_size.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.quizz_size.FormattingEnabled = true;
            this.quizz_size.Location = new System.Drawing.Point(12, 318);
            this.quizz_size.Name = "quizz_size";
            this.quizz_size.Size = new System.Drawing.Size(350, 21);
            this.quizz_size.TabIndex = 4;
            // 
            // reg_nbr
            // 
            this.reg_nbr.AutoSize = true;
            this.reg_nbr.Location = new System.Drawing.Point(9, 138);
            this.reg_nbr.Name = "reg_nbr";
            this.reg_nbr.Size = new System.Drawing.Size(50, 13);
            this.reg_nbr.TabIndex = 11;
            this.reg_nbr.Text = "Matricule";
            // 
            // reg_nbr_box
            // 
            this.reg_nbr_box.Location = new System.Drawing.Point(12, 154);
            this.reg_nbr_box.Name = "reg_nbr_box";
            this.reg_nbr_box.Size = new System.Drawing.Size(350, 20);
            this.reg_nbr_box.TabIndex = 2;
            // 
            // name_error
            // 
            this.name_error.AutoSize = true;
            this.name_error.ForeColor = System.Drawing.Color.DarkRed;
            this.name_error.Location = new System.Drawing.Point(9, 52);
            this.name_error.Name = "name_error";
            this.name_error.Size = new System.Drawing.Size(129, 13);
            this.name_error.TabIndex = 13;
            this.name_error.Text = "Renseignez votre Prénom";
            this.name_error.Visible = false;
            // 
            // email_error
            // 
            this.email_error.AutoSize = true;
            this.email_error.ForeColor = System.Drawing.Color.DarkRed;
            this.email_error.Location = new System.Drawing.Point(9, 115);
            this.email_error.Name = "email_error";
            this.email_error.Size = new System.Drawing.Size(118, 13);
            this.email_error.TabIndex = 14;
            this.email_error.Text = "Renseignez votre NOM";
            this.email_error.Visible = false;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 20;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // logoHolder
            // 
            this.logoHolder.Image = ((System.Drawing.Image)(resources.GetObject("logoHolder.Image")));
            this.logoHolder.Location = new System.Drawing.Point(383, 259);
            this.logoHolder.Name = "logoHolder";
            this.logoHolder.Size = new System.Drawing.Size(337, 80);
            this.logoHolder.TabIndex = 15;
            this.logoHolder.TabStop = false;
            this.logoHolder.Visible = false;
            // 
            // mandatoryFields
            // 
            this.mandatoryFields.AutoSize = true;
            this.mandatoryFields.Location = new System.Drawing.Point(12, 177);
            this.mandatoryFields.Name = "mandatoryFields";
            this.mandatoryFields.Size = new System.Drawing.Size(107, 13);
            this.mandatoryFields.TabIndex = 16;
            this.mandatoryFields.Text = "* champs obligatoires";
            // 
            // level_selector
            // 
            this.level_selector.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.level_selector.FormattingEnabled = true;
            this.level_selector.Location = new System.Drawing.Point(12, 270);
            this.level_selector.Name = "level_selector";
            this.level_selector.Size = new System.Drawing.Size(350, 21);
            this.level_selector.TabIndex = 17;
            // 
            // level
            // 
            this.level.AutoSize = true;
            this.level.Location = new System.Drawing.Point(9, 254);
            this.level.Name = "level";
            this.level.Size = new System.Drawing.Size(118, 13);
            this.level.TabIndex = 18;
            this.level.Text = "Sélectionnez un niveau";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(368, 326);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(222, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Copyright © 2016 Ciprel | Tous droits réservés";
            // 
            // InitForm
            // 
            this.AcceptButton = this.start;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(730, 348);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.level);
            this.Controls.Add(this.level_selector);
            this.Controls.Add(this.mandatoryFields);
            this.Controls.Add(this.email_error);
            this.Controls.Add(this.name_error);
            this.Controls.Add(this.reg_nbr_box);
            this.Controls.Add(this.reg_nbr);
            this.Controls.Add(this.quizz_size);
            this.Controls.Add(this.quizzSize);
            this.Controls.Add(this.quit);
            this.Controls.Add(this.start);
            this.Controls.Add(this.category_selector);
            this.Controls.Add(this.category);
            this.Controls.Add(this.lastNameBox);
            this.Controls.Add(this.email);
            this.Controls.Add(this.name);
            this.Controls.Add(this.firstNameBox);
            this.Controls.Add(this.logoHolder);
            this.Controls.Add(this.logoCiprel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "InitForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "QCM Ciprel";
            this.Load += new System.EventHandler(this.Init_Load);
            ((System.ComponentModel.ISupportInitialize)(this.logoCiprel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.logoHolder)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox firstNameBox;
        private System.Windows.Forms.Label name;
        private System.Windows.Forms.Label email;
        private System.Windows.Forms.TextBox lastNameBox;
        private System.Windows.Forms.Label category;
        private System.Windows.Forms.ComboBox category_selector;
        private System.Windows.Forms.PictureBox logoCiprel;
        private System.Windows.Forms.Button start;
        private System.Windows.Forms.Button quit;
        private System.Windows.Forms.Label quizzSize;
        private System.Windows.Forms.ComboBox quizz_size;
        private System.Windows.Forms.Label reg_nbr;
        private System.Windows.Forms.TextBox reg_nbr_box;
        private System.Windows.Forms.Label name_error;
        private System.Windows.Forms.Label email_error;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label mandatoryFields;
        private System.Windows.Forms.PictureBox logoHolder;
        private System.Windows.Forms.ComboBox level_selector;
        private System.Windows.Forms.Label level;
        private System.Windows.Forms.Label label1;
    }
}