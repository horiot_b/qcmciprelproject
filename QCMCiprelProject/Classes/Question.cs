﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace QCMCiprelProject
{
    /// <summary>
    ///     Différents états possibles d'une question servant à déterminer, entre autres,
    ///     le nombre de questions restantes ou les questions auquelles l'utilisateur n'a pas répondu
    ///     // Different possible state of a question, used for example to know the number of remaining questions or
    ///     the skipped questions
    /// </summary>
    public enum State { New, Answered, Skipped, Unanswered };

    /// <summary>
    ///     Question d'un questionnaire // Questions of the quizz
    /// </summary>
    public class Question
    {
        /// <summary>
        ///     Identifiant unique // Unique identifiant
        /// </summary>
        public long id { get; set; }

        /// <summary>
        ///     Contenu (texte) de cette question // Question's content
        /// </summary>
        public String question { get; set; }

        /// <summary>
        ///     Commentaire apparaissant pendant que l'utilisateur répond à la question
        ///     // Comment displayed while the user is answering the question
        /// </summary>
        public String commentQuestion { get; set; }

        /// <summary>
        ///     Commentaire apparaissant si l'utilisateur répond correctement à cette quesion
        ///     // Comment displayed if the user gave the correct answer
        /// </summary>
        public String commentCorrectAnswer { get; set; }

        /// <summary>
        ///     Commentaire apparaisant si l'utilisateur ne répond pas correctement à cette question
        ///     // Comment displayed if the user gave an incorrect answer
        /// </summary>
        public String commentIncorrectAnswer { get; set; }

        /// <summary>
        ///     Image pouvant accompagner la question // Optionnal image
        /// </summary>
        public Image image { get; set; }

        /// <summary>
        ///     Liste des réponses possibles à cette question // List of possible answers for this question
        /// </summary>
        public List<Answer> answers { get; set; }

        /// <summary>
        ///     Etat actuel de la question, se référer à la documentation de l'enum 'State' pour plus d'informations
        ///     // Current state of the question, refer to the 'State' enum documentation for more information
        /// </summary>
        public State state { get; set; }

        /// <summary>
        ///     Une question normale aura entre 2 et 4 réponses possibles tandis qu'une question de type 'ErrorGame'
        ///     consite à avoir une seule image cliquable en terme de réponse
        ///     // A normal question will have between 2 and 4 possible answers wheras a question of type 'ErrorGame'
        ///     got only one clickable picture as answer
        /// </summary>
        public bool isErrorGame { get; set; }

        /// <summary>
        ///     Constructeur avec paramètres // Constructor with parameters
        /// </summary>
        /// <param name="id"></param>
        /// <param name="question"></param>
        /// <param name="commentQuestion"></param>
        /// <param name="commentCorrectAnswer"></param>
        /// <param name="commentIncorrectAnswer"></param>
        /// <param name="image"></param>
        /// <param name="isErrorGame"></param>
        /// <param name="answers"></param>
        public Question(long id, String question, String commentQuestion, String commentCorrectAnswer,
            String commentIncorrectAnswer, Image image, bool isErrorGame, List<Answer> answers)
        {
            this.id = id;
            this.question = question;
            this.commentQuestion = commentQuestion;
            this.commentCorrectAnswer = commentCorrectAnswer;
            this.commentIncorrectAnswer = commentIncorrectAnswer;
            this.image = image;
            this.isErrorGame = isErrorGame;
            this.answers = new List<Answer>(answers);

            state = State.New;
        }

        /// <summary>
        ///     Place les réponses dans un ordre aléatoire // Suffles the order of the answers
        /// </summary>
        public void shuffleAnswers()
        {
            Random rng = new Random();

            int n = this.answers.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                Answer value = this.answers[k];
                this.answers[k] = this.answers[n];
                this.answers[n] = value;
            }
        }
    }
}
