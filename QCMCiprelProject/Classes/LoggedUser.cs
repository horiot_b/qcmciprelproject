﻿namespace QCMCiprelProject
{
    /// <summary>
    ///     Informations sur l'utilisateur faisant le questionnaire // User's data
    /// </summary>
    public class LoggedUser
    {
        /// <summary>
        ///     Prénom NOM // Firstname LASTNAME
        /// </summary>
        public string firstName { get; set; }

        /// <summary>
        ///     Adresse email // Email address
        /// </summary>
        public string lastName { get; set; }

        /// <summary>
        ///     Matricule // Registration number
        /// </summary>
        public string regNbr { get; set; }

        /// <summary>
        ///     Constructeur avec paramètres // Constructor with parameters
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="regNbr"></param>
        public LoggedUser(string firstName, string lastName, string regNbr)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.regNbr = regNbr;
        }
    }
}
