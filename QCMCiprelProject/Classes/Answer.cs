﻿using System;

namespace QCMCiprelProject
{
    /// <summary>
    ///     Réponse à une question // Answer to a question
    /// </summary>
    public class Answer
    {
        /// <summary>
        ///     Identifiant unique // Unique identifiant
        /// </summary>
        public long id { get; set;  }

        /// <summary>
        ///     Identifiant de la question à laquelle cette réponse est rattachée // Id of the question for which this answer corresponds
        /// </summary>
        public long questionId { get; set; }

        /// <summary>
        ///     Contenu (texte) de la réponse // Content of the answer
        /// </summary>
        public String content { get; set; }

        /// <summary>
        ///     Est-ce la bonne réponse? // Is it the good answer?
        /// </summary>
        public bool correctAnswer { get; set; }

        /// <summary>
        ///     Constructeur avec paramètres // Constructor with parameters
        /// </summary>
        /// <param name="id"></param>
        /// <param name="questionId"></param>
        /// <param name="content"></param>
        /// <param name="correctAnswer"></param>
        public Answer(long id, long questionId, String content, bool correctAnswer)
        {
            this.id = id;
            this.questionId = questionId;
            this.content = content;
            this.correctAnswer = correctAnswer;
        }
    }
}
