﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Resources;
using System.Windows.Forms;

namespace QCMCiprelProject
{
    /// <summary>
    ///     Objet contenant les questions du questionnaire en cours et les affichant dans un ordre aléatoire
    ///     // Contains the quizz's questions and gives them in a random order
    /// </summary>
    class Quizz
    {
        /// <summary>
        ///     Liste des questions du questionnaire // Quizz's question's list
        /// </summary>
        private List<Question> questions;
        
        /// <summary>
        ///     Constructeur avec paramètres // Constructor with parameters
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="level"></param>
        /// <param name="size"></param>
        public Quizz(long categoryId, int level, int size)
        {
            List<Answer> answers = new List<Answer>();
            questions = new List<Question>();

            int nbrQuestions = 0;

            // Etablie le nombre de questions que le questionnaire contient // Select the amount of questions the quizz will have
            switch (size)
            {
                case 1:
                    nbrQuestions = 10;
                    break;
                case 2:
                    nbrQuestions = 20;
                    break;
                case 3:
                    nbrQuestions = 40;
                    break;
            }

            // Initialisation des éléments permettant de faire une requête sur la base de données // Initialisation of the requiered objects
            // to do a request in the database
            try
            {
                SqlConnection conn = new SqlConnection(Properties.Resources.DatabaseConnectionString2);
                SqlDataAdapter da = new SqlDataAdapter();
                DataSet dsQuestions = new DataSet();
                DataSet dsAnswers = new DataSet();
                DataTable dtQuestions = new DataTable();
                DataTable dtAnswers = new DataTable();

                SqlCommand command = new SqlCommand(@"SELECT * FROM Question WHERE categoryId = @CATEGORY AND level = @LEVEL ORDER BY RAND()", conn);

                SqlParameter categoryParam = new SqlParameter();
                categoryParam.ParameterName = "@CATEGORY";
                categoryParam.Value = categoryId;

                SqlParameter levelParam = new SqlParameter();
                levelParam.ParameterName = "@LEVEL";
                levelParam.Value = level;

                command.Parameters.Add(categoryParam);
                command.Parameters.Add(levelParam);

                da.SelectCommand = command;

                da.Fill(dsQuestions, 0, nbrQuestions, "Question");
                dtQuestions = dsQuestions.Tables["Question"];

                // Remplissage de la liste avec les données de la base // Filling the list with data
                foreach (DataRow dr in dtQuestions.Rows)
                {
                    answers.Clear();
                    dsAnswers.Clear();

                    long questionId = long.Parse(dr["id"].ToString());

                    da.SelectCommand = new SqlCommand(@"SELECT * FROM Answer WHERE questionId = " + questionId, conn);
                    da.Fill(dsAnswers, "Answer");
                    dtAnswers = dsAnswers.Tables["Answer"];

                    foreach (DataRow drAnswer in dtAnswers.Rows) answers.Add(new Answer(long.Parse(drAnswer["id"].ToString()),
                        questionId, drAnswer["content"].ToString(), bool.Parse(drAnswer["correctAnswer"].ToString())));

                    // Récupération de l'image facultative // Getting the optionnal image
                    Image image = null;
                    if (dr["image"].ToString().Length > 0) image = Image.FromStream(new MemoryStream((byte[])dr["image"]));

                    questions.Add(new Question(questionId, dr["content"].ToString(),
                        dr["commentQuestion"].ToString(), dr["commentCorrectAnswer"].ToString(),
                        dr["commentIncorrectAnswer"].ToString(), image, bool.Parse(dr["isErrorGame"].ToString()), answers));
                }

                // Fermeture de la connexion à la base de données // Closing the connection with the database
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter au server", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.Write(ex);
            }

        }


        /// <summary>
        ///     Renvoie une question aléatoire se trouvant dans l'état donné // Give a random question in the asked state
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        public Question getRandomQuestionByState(State state)
        {
            List<Question> newQuestions = new List<Question>();

            // Etablisement d'une liste ne contenant que les question dans l'état donné
            foreach (Question question in questions) if (question.state.Equals(state)) newQuestions.Add(question);

            if (newQuestions.Count.Equals(0)) return null;

            // Renvoie une question aléaoire dans la liste
            return newQuestions.ElementAt(new Random().Next(newQuestions.Count));
        }

        /// <summary>
        ///     Change une question d'état // Change de state of a given question
        /// </summary>
        /// <param name="questionId"></param>
        /// <param name="state"></param>
        public void changeQuestionState(long questionId, State state)
        {
            List<Question> newList = new List<Question>();

            foreach (Question question in questions)
            {
                if (question.id.Equals(questionId)) question.state = state;
                newList.Add(question);
            }
            questions = newList;
        }

        /// <summary>
        ///     Renvoie la taille du questionnaire // Give the quizz size
        /// </summary>
        /// <returns></returns>
        public int getSize()
        {
            return questions.Count();
        }
    }
}
