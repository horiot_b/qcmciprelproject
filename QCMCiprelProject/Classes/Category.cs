﻿using System;

namespace QCMCiprelProject
{
    /// <summary>
    ///     Catégorie (sujet) de questions // Category of questions
    /// </summary>
    public class Category
    {
        /// <summary>
        ///     Identifiant unique // Unique identifiant
        /// </summary>
        public long id { get; set; }

        /// <summary>
        ///     Nom de cette catégorie // Category's name
        /// </summary>
        public String name { get; set; }

        /// <summary>
        ///     Constructeur avec paramètres // Constructor with parameters
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        public Category(long id, String name)
        {
            this.id = id;
            this.name = name;
        }
    }
}
